var mysql = require('../../lib/mysqlConnection/MysqlConnection'),
	config = require('../../config/config');

module.exports = function (app) {

	var UserRepository = new(require('./repositories/UserRepository'))(mysql),
		UserController = new (require('./controllers/UserController'))(UserRepository, app);

	return require ('./routes/UserRoutes')(UserController);
};