var express = require('express');
var router = express.Router();
var jwtauth = require('.././jwtAuth.js');

module.exports = function(UserController){
    router.post('/login', UserController.login);
    router.post('/register', UserController.register);
    router.post('/subscribe', UserController.subscribe);
    router.get('/subscribers', jwtauth, UserController.getSubscribers);
    return router;
};
