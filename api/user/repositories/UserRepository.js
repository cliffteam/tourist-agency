var Q = require('q');
var bcrypt = require('bcryptjs');
var moment = require('moment');
module.exports = function UserRepository(mysql, config) {


    function loginUser(email, pass){
        var query = [
            'SELECT id, email, password FROM users',
            'WHERE email=:email'
        ].join(" ");

        return mysql.makeQuery(query, {
            email: email,
            pass: pass,
        }, function(users){
            if (users.length === 0) {
                return false;
            }

            var loggedUser = null;
            var hash = '';
            users.forEach(function(user){
                if (bcrypt.compareSync(pass, user.password)) {
                    loggedUser = {
                        userId: user.id,
                        email: user.email
                    };
                    return false;
                }
            });
            return loggedUser;

        });
    }

    function registerUser(userData){
        var query = [
            'INSERT INTO `users` (`email`, `password`)',
            'VALUES (:email, :password);'
        ].join(" ");

        // var salt = bcrypt.genSaltSync(10);
        var hash = bcrypt.hashSync(userData.password);

        return mysql.makeQuery(query, {
            email: userData.email,
            password: hash
        }, function (result) {
            return {
                userId: result.insertId,
                email: userData.email,
            };
        });
    }

    function checkIfUserExists(email, returnId){
        var query = [
            'SELECT * FROM users',
            'WHERE email=:email'
        ].join(" ");

        return mysql.makeQuery(query, {
            email: email,
        }, function (result) {

            if (returnId === true) {
                return {
                    isExisting: result && result.length > 0,
                    userId: result[0] ? result[0].id : false
                };
            }
            else{
                return result && result.length > 0;
            }
        });
    }

    function addSubscriber(email){
        var query = [
            'INSERT INTO `newsletter` (`email`)',
            'VALUES (:email);'
        ].join(" ");

        return mysql.makeQuery(query, {
            email: email
        }, function (result) {
            return {
                id: result.insertId,
                email: email,
            };
        });
    }

    function getSubscribers(){
        var query = [
            'SELECT * FROM newsletter'
        ].join(" ");

        return mysql.makeQuery(query, null, function (result) {
            return result;
        });
    }

    function findUser(userId){
        var query = [
            'SELECT * FROM users',
            'WHERE id=:userId'
        ].join(" ");

        return mysql.makeQuery(query, {
            userId: userId,
        }, function (user) {
            return user;
        });
    }

    return {
        loginUser: loginUser,
        registerUser: registerUser,
        checkIfUserExists: checkIfUserExists,
        addSubscriber: addSubscriber,
        getSubscribers: getSubscribers,
        findUser: findUser
    };
}