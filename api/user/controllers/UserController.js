var fs = require('fs');
var Q = require('q');
var nodemailer = require('nodemailer');
var moment = require('moment');
var config = require('../../../config/config');
var jwt = require('jwt-simple');

module.exports = function UserController(UserRepository, app) {

    function login(req, res, next){
        UserRepository.loginUser(req.body.email, req.body.password).then(function(user){
            if (user) {
                var expires = moment().add(7, 'days').valueOf();
                var secret = app.get('jwtTokenSecret');
                var token = jwt.encode(
                    {
                        iss: user.userId,
                        exp: expires
                    },
                    app.get('jwtTokenSecret')
                );

                res.json({
                    success: true,
                    user: user,
                    token: token,
                    expires: expires
                });
            }
            else{
                res.status(401).json({
                    success: false,
                    msg: "Wrong user or password"
                });
            }
        });
    }
    function register(req, res, next){
        let error = null;

        let isValidEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(req.body.email);
        if (!req.body.email || !isValidEmail) {
            error = "Моля, въведете валиден E-mail адрес";
        } else if(!req.body.password){
            error = "Моля, въведете парола";
        } /*else if(!req.body.name){
            // error = "Моля, въведете име и фамилия";
        }*/
        if (error) {
            res.json({
                success: false,
                msg: error
            });
            return;
        }
        console.log("REACHED", req.body);
        UserRepository.checkIfUserExists(req.body.email).then(function(isExisting){
            if (isExisting === true) {
                res.json({
                    success: false,
                    msg: "User already exists"
                });
            }
            else{
                UserRepository.registerUser({
                    email: req.body.email,
                    password: req.body.password,
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    companyname: req.body.companyname,
                    phoneNumber: req.body.phoneNumber
                }).then(function(user){
                    res.json({
                        success: true,
                        user: user
                    });
                }).catch(function(err){
                    console.log(err);
                    res.json({success:false, msg: err.code});
                });
            }
        });
    }

    function subscribe(req, res){
        UserRepository.addSubscriber(req.body.email).then(function(result){
            res.json({subscriber: result});
        }).catch(function(err){
            console.log(err);
            res.json({success:false, msg: err.code});
        });
    }

    function getSubscribers(req, res){
        UserRepository.getSubscribers().then(function(result){
            res.json(result);
        }).catch(function(err){
            console.log(err.code);
            res.json({success:false, msg: err});
        });
    }

    function _sendEmails(mailOptions, callback){
        var smtpConfig = {
            host: 'giga.superhosting.bg',
            requireTLS: true, //Force TLS
            port: 465,
            secure: true, // use SSL
            auth: {
                user: '',
                pass: ''
            }
        };
        var transporter = nodemailer.createTransport(smtpConfig);

        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
                callback(false);
            }
            callback(true);
        });
    };
    function sendMassEmail (req, res){
        let data = req.body,
            method = null;

        var mailOptions = {
            from: 'martin.lazarov2@gmail.com', // sender address
            to: 'martin@mslworkshop.com', // list of receivers
            subject: data.subject, // Subject line
            text: data.content, // plaintext body
            // html: '<b>Hello world 🐴</b>' // html body
        };
        if (method === null && (!data.userslist || data.userslist.length === 0)) {
            res.json({
                success: false,
                data: data.userslist
            });
        }
        else if (data.userslist.length > 0 && data.emailType === 'userslist') {
            console.log(this._sendEmails, _sendEmails);
            mailOptions.to = data.userslist;
            _sendEmails(mailOptions, (result) => {
                res.json({
                    success: result
                });
            });
        }
        else{
            UserRepository.getSubscribers().then(function(result){
                var emails = result.map(a => a.email);
                mailOptions.to = emails;
                _sendEmails(mailOptions, (result) => {
                    res.json({
                        success: result
                    });
                });
            });
        }

    };

    return {
        login: login,
        register: register,
        subscribe: subscribe,
        getSubscribers: getSubscribers
    };
};