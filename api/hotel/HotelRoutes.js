const express = require('express');
const router = express.Router();
const jwtauth = require('../user/jwtAuth.js');
const multer = require('multer');
const appConfig = require('../../config/main.config');
const fileService = new (require('../../lib/FileService'))();

const storage = fileService.createStorage(multer, appConfig.UPLOADS_PATH_HOTEL);

const upload = multer({ storage: storage });
const hotelUpload = upload.fields([{ name: 'photos', maxCount: 20 }]);

module.exports = function (HotelController) {
    router.get('/', jwtauth, HotelController.getList);
    router.get('/:id', jwtauth, HotelController.getById);
    router.post('/', jwtauth, hotelUpload, HotelController.create);
    router.put('/:id', jwtauth, hotelUpload, HotelController.update);
    router.delete('/:id', jwtauth, HotelController.remove);
    router.delete('/photos/:id', jwtauth, HotelController.deletePhoto);

    return router;
};
