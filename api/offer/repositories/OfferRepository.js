var moment = require('moment');
var Utils = require('../../../lib/utils');
var async = require('async');

module.exports = function OfferRepository(mysql, config) {

    function getAllCategories() {
        var query = [
            'SELECT id, name_bg as nameBg, name_en as nameEn, name_ru as nameRu FROM categories'
        ].join(" ");

        return mysql.makeQuery(query, null, function (result) {
            return result
        });
    }

    function getAllSubcategories(categoryId) {
        var query = [
            'SELECT id, name_bg as nameBg, name_en as nameEn, name_ru as nameRu, image_url as imageUrl, categories_id FROM subcategories'
        ].join(" ");

        if (categoryId) {
            query += ' WHERE categories_id = :categoryId'
        }

        return mysql.makeQuery(query, {categoryId}, function (result) {
            return result
        });
    }

    function getSubcategoryById(subcatId) {
        var query = [
            'SELECT id, name_bg as nameBg, name_en as nameEn, name_ru as nameRu, image_url as imageUrl , categories_id as categoryId FROM subcategories',
            'WHERE id = :subcatId'
        ].join(" ");

        return mysql.makeQuery(query, {
            subcatId
        }, function (result) {
            return result.shift() || null;
        });
    }

    function addSubcategory(data) {
        var query = [
            'INSERT INTO `subcategories` (`name_bg`, `name_en`, `name_ru`, `image_url`, `categories_id`)',
            'VALUES (:nameBg, :nameEn, :nameRu, :imagePath, :categoryId);'
        ].join(" ");

        return mysql.makeQuery(query, {
            nameBg: data.nameBg,
            nameEn: data.nameEn,
            nameRu: data.nameRu,
            imagePath: data.imagePath,
            categoryId: data.categoryId
        }, function (result) {
            return {
                id: result.insertId
            };
        });
    }

    function updateSubcategory(subcategoryId, data) {
      const imageUrlQuery = data.imageUrl ? ', image_url = :imageUrl' : '';
        const query = [
            'UPDATE subcategories',
            'SET name_en = :nameEn, name_bg = :nameBg, name_ru = :nameRu, categories_id = :categoryId' + imageUrlQuery,
            'WHERE id = :subcategoryId'
        ].join(" ");

        return mysql.makeQuery(query, {
            subcategoryId: subcategoryId,
            nameBg: data.nameBg,
            nameEn: data.nameEn,
            nameRu: data.nameRu,
            categoryId: data.categoryId,
            imageUrl: data.imageUrl
        }, function (result) {
            return result.affectedRows > 0;
        }, true);
    }

    function deleteSubcategory(subcategoryId) {
        var query = [
            'DELETE FROM `subcategories`',
            'WHERE id=:subcategoryId;'
        ].join(" ");

        return mysql.makeQuery(query, {
            subcategoryId
        }, function (result) {
            return result.affectedRows > 0;
        });
    }

    function addOfferTranslations(offerId, translations) {

        let query = [
            'INSERT INTO `offer_translation`',
            '(`offer_id`, `language_code`, `name`, `schedule`, `itinerary`, `promo_title`, `duration`, `pricing`, `subtitle`, `other_info`)',
            'VALUES '
        ].join(" ");

        let values = [];
        translations.forEach(t => {
            values.push('(' + [offerId, Utils.quotedString(t.languageCode), Utils.quotedString(t.name), Utils.quotedString(t.schedule), Utils.quotedString(t.itinerary), Utils.quotedString(t.promoTitle), Utils.quotedString(t.duration), Utils.quotedString(t.pricing), Utils.quotedString(t.subtitle), Utils.quotedString(t.otherInfo)].join(', ') + ')');
        });

        query += values.join(', ') + ';';
        return mysql.makeQuery(query, null, function (result) {
            return result;
        });
    }

    function deleteOfferTranslations(offerId) {
        const query = [
            'DELETE FROM `offer_translation`',
            'WHERE offer_id=:offerId;'
        ].join(" ");

        return mysql.makeQuery(query, {
            offerId: offerId
        }, function (result) {
            return result.warningCount == 0;
        });
    }

    function deleteOffer(offerId) {
        var query = [
            'DELETE FROM `offers`',
            'WHERE id=:offerId;'
        ].join(" ");

        return mysql.makeQuery(query, {
            offerId: offerId
        }, function (result) {
            return result.warningCount == 0;
        });
    }

    function addOfferPhotos(offerId, photos) {
        var query = [
            'INSERT INTO `offer_photos` (`offer_id`, `path`)',
            'VALUES '
        ].join(" ");

        var values = [];
        photos.forEach(photo => {
            values.push(`('${offerId}', '${photo.file}')`);
        });

        query += values.join(', ') + ';';

        return mysql.makeQuery(query, null, function (result) {
            return result;
        });
    }

    function deletePhoto(tableName, id) {
        var query = [
            'DELETE FROM `' + tableName + '_photos`',
            'WHERE id=:id;'
        ].join(" ");

        return mysql.makeQuery(query, { id }, function (result) {
            return result.warningCount == 0;
        });
    }

    function addPhotos(tableName, id, photos) {
        var query = [
            'INSERT INTO `' + tableName + '_photos` (`' + tableName + '_id`, `path`)',
            'VALUES '
        ].join(" ");

        var values = [];
        photos.forEach(photo => {
            values.push(`('${id}', '${photo}')`);
        });

        query += values.join(', ') + ';';

        return mysql.makeQuery(query, null, function (result) {
            return result.warningCount == 0;
        });
    }

    function deleteOfferPhotos(offerId) {
        var query = [
            'DELETE FROM `offer_photos`',
            'WHERE offer_id=:offerId;'
        ].join(" ");

        return mysql.makeQuery(query, {
            offerId: offerId
        }, function (result) {
            return result.warningCount == 0;
        });
    }

    function addOfferTravelDates(offerId, dates) {
        var query = [
            'INSERT INTO `offer_travel_dates` (`offer_id`, `date_from`, `date_to`)',
            'VALUES '
        ].join(" ");

        var values = [];
        dates.forEach(d => {
            values.push('(' + [
                offerId,
                Utils.quotedString(moment(d.dateFrom).format('YYYY-MM-DD').toString()),
                Utils.quotedString(moment(d.dateTo).format('YYYY-MM-DD').toString()),
            ].join(', ') + ')');
        });

        query += values.join(', ') + ';';

        return mysql.makeQuery(query, null, function (result) {
            return result;
        });
    }

    function deleteOfferTravelDates(offerId) {
        var query = [
            'DELETE FROM `offer_travel_dates`',
            'WHERE offer_id=:offerId;'
        ].join(" ");

        return mysql.makeQuery(query, {
            offerId: offerId
        }, function (result) {
            return result.warningCount == 0;
        });
    }

    function getOfferTravelDates(offerId) {
        const query = [
            'SELECT id, offer_id as offerId, date_from as dateFrom, date_to as dateTo FROM offer_travel_dates',
            'WHERE offer_id = :offerId'
        ].join(" ");

        return mysql.makeQuery(query, {
            offerId: offerId
        }, function (result) {
            return result;
        });
    }

    function getTravelDatesByListOfOfferIds(offerIdList) {
        const query = [
            'SELECT id, offer_id as offerId, date_from as dateFrom, date_to as dateTo FROM offer_travel_dates',
            `WHERE offer_id IN (${offerIdList.length ? offerIdList.join(', ') : -1})`
        ].join(" ");

        return mysql.makeQuery(query, null, (result) => {
            return result;
        });
    }

    function getOfferPhotos(offerId) {
        var query = [
            'SELECT id, offer_id as offerId, path FROM offer_photos',
            'WHERE offer_id = :offerId'
        ].join(" ");

        return mysql.makeQuery(query, {
            offerId: offerId
        }, function (result) {
            return result;
        });
    }

    function getPhotos(tableName, id) {
        var query = [
            `SELECT id, ${tableName}_id as ${tableName}Id, path FROM ${tableName}_photos`,
            `WHERE ${tableName}_id = :id`
        ].join(" ");

        return mysql.makeQuery(query, {
            id: id
        }, function (result) {
            return result;
        });
    }

    function getOfferTranslations(offerId, lang) {
        let query = [
            `SELECT id,
                offer_id as offerId,
                language_code as languageCode,
                name,
                subtitle,
                schedule,
                itinerary,
                promo_title AS promoTitle,
                pricing,
                duration,
                other_info AS otherInfo
                FROM offer_translation`,
            'WHERE offer_id = :offerId'
        ].join(" ");

        if (lang) {
            query += ` AND language_code = '${lang}'`
        }

        return mysql.makeQuery(query, {
            offerId: offerId
        }, function (result) {
            return result;
        });
    }

    function addOffer(data) {

        const query = [
            'INSERT INTO `offers` (`transport_type_id`, `subcategories_id`,',
            '`ref_id`, `offer_type`, `price`, `is_promo`, `promo_price`, `head_photo_index`, `is_active`, `level_slider`, `level_subcategory`)',
            'VALUES (:transportType, :subcategoryId, :refId, :type, :price, :isPromo, :promoPrice, :headPhotoIndex, :isActive, :sliderLevel, :subcategoryLevel);'
        ].join(" ");

        return mysql.makeQuery(query, data, function (result) {
            return {
                id: result.insertId
            };
        }, true);
    }

    function getOffer(offerId) {
        var query = [
            'SELECT * FROM offers',
            'WHERE id = :offerId'
        ].join(" ");

        return mysql.makeQuery(query, {
            offerId: offerId
        }, function (result) {
            const offer = result.shift();
            return formatOffer(offer);
        });
    }

    function formatOffer(offer) {
        if (offer) {
            offer.subcategoryId = offer.subcategories_id;
            offer.created_at = offer.created_at;
            offer.transportType = offer.transport_type_id;
            offer.offerType = offer.offer_type;
            offer.isPromo = offer.is_promo > 0;
            offer.isActive = offer.is_active > 0;
            offer.promoPrice = offer.promo_price;
            offer.refId = offer.ref_id;
            offer.headPhotoIndex = offer.head_photo_index;
            offer.sliderLevel = offer.level_slider;
            offer.subcategoryLevel = offer.level_subcategory;

            const removeFieldsList = [
                'is_promo', 'is_active', 'ref_id', 'promo_price',
                'categories_id', 'created_at', 'destination_from_id',
                'destination_to_id', 'transport_type_id', 'offer_type'
            ];

            removeFieldsList.forEach(fieldKey => {
                offer[fieldKey] = undefined;
            });
        }
        return offer;
    }

    function getOffersByFlag(flag, subcatId) {
        var query = [
            'SELECT id, subcategories_id as subcategoryId,',
            'price,',
            'created_at as createdAt FROM offers WHERE',
            flag,
            '= 1'
        ].join(" ");

        if (subcatId) {
            query += ' AND subcategories_id = :subcatId'
        }

        return mysql.makeQuery(query, {
            subcatId: subcatId,
            flag: flag
        }, function (result) {
            return result
        });
    }

    function getOffers(subcatId) {
        let query = 'SELECT * FROM offers';

        if (subcatId) {
            query += ' WHERE subcategories_id = :subcatId ORDER BY level_subcategory DESC'
        }

        return mysql.makeQuery(query, {
            subcatId
        }, function (result) {
            return result.map(formatOffer);
        });
    }

    function executeOffersByCountryQuery(countryId) {
        let query = [
            'SELECT O.* FROM offers as O LEFT JOIN offer_has_destination as OHD',
            'ON OHD.offers_id = O.id LEFT JOIN destination as D',
            'ON D.id = OHD.destination_id',
            'LEFT JOIN country ON country.id = D.country_id',
            'WHERE country.id = :countryId'
        ].join(" ");

        return mysql.makeQuery(query, {
            countryId
        }, function (result) {
            return result.map(formatOffer);
        });
    }

    function executeOffersByDestinationQuery(destinationId) {
        let query = [
            'SELECT O.* FROM offers as O INNER JOIN offer_has_destination as OHD',
            'ON OHD.offers_id = O.id',
            'WHERE OHD.destination_id = :destinationId'
        ].join(" ");

        return mysql.makeQuery(query, {
            destinationId
        }, function (result) {
            return result.map(formatOffer);
        });
    }

    function getOffersByCountry(countryId, lang) {
        return new Promise(async function (resolve, reject) {
            try {
                let result = await executeOffersByCountryQuery(countryId);
                let offers = await buildOffersData(result, lang);
                resolve(offers);
            } catch (error) {
                reject(error);
            }
        });
    }

    function getOffersByDestination(destinationId, lang) {
        return new Promise(async function (resolve, reject) {
            try {
                let result = await executeOffersByDestinationQuery(destinationId);
                let offers = await buildOffersData(result, lang);
                resolve(offers);
            } catch (error) {
                reject(error);
            }
        });
    }

    function updateOffer(offerId, data) {
        const query = [
            'UPDATE offers',
            `SET subcategories_id = :subcategoryId, price = :price, promo_price = :promoPrice, transport_type_id = :transportType, `,
            `ref_id = :refId, offer_type = :type, price = :price, is_promo = :isPromo, `,
            'head_photo_index = :headPhotoIndex, is_active = :isActive,',
            'level_slider = :sliderLevel, level_subcategory = :subcategoryLevel',
            'WHERE id = :offerId'
        ].join(" ");
        data.offerId = offerId;

        return mysql.makeQuery(query, data, function (result) {
            return result.affectedRows > 0;
        });
    }

    function deleteAllOffersBySubcategoryId(subcatId) {
        const query = [
            'DELETE FROM `offers`',
            'WHERE subcategories_id=:subcatId;'
        ].join(" ");

        return mysql.makeQuery(query, {
            subcatId
        }, function (result) {
            return result.warningCount == 0;
        });
    }

    //Methods for the public pages
    function getHomePageData() {
        var offers = {
            promo: [],
            top: [],
            best: []
        }
        async.parallel({
            // top: Utils.asyncFromPromise(getOffersByFlag('is_top_offer')),
            // promo: Utils.asyncFromPromise(getOffersByFlag('is_urgent')),
            best: Utils.asyncFromPromise(getOffers())
        }, function (err, results) {
            var doneCats = 0;
            var callback = el => {
                doneCats++;
                if (doneCats == 3) {
                    return offers;
                }
            }
            /*results.top.forEach(el => {
                attachDataToOffer(el, offers.top, callback);
            });
            results.promo.forEach(el => {
                attachDataToOffer(el, offers.promo, callback);
            })*/
            results.best.forEach(el => {
                attachDataToOffer(el, offers.best, callback);
            })

        })

    }

    function buildOffersData(rawOffers, lang) {
        return new Promise(async function (resolve, reject) {
            try {
                var offers = [];
                let result = rawOffers;
                if (result.length && result.length > 0) {
                    result.forEach(el => {
                        async.parallel({
                            dates: Utils.asyncFromPromise(getOfferTravelDates(el.id)),
                            translations: Utils.asyncFromPromise(getOfferTranslations(el.id, lang)),
                            photos: Utils.asyncFromPromise(getOfferPhotos(el.id))
                        }, function (err, results) {
                            if (err) {
                                reject(err);
                            }
                            el.dates = results.dates;
                            el.translations = results.translations;
                            el.photos = results.photos;
                            offers.push(el);
                            if (offers.length === result.length) {
                                resolve(offers);
                            }
                        });
                    });
                } else {
                    resolve([]);
                }
            } catch (error) {
                reject(error);
            }
        });
    }

    async function getOffersBySubcategory(subcategory, lang) {

        return new Promise(async function (resolve, reject) {
            try {
                let result = await getOffers(subcategory);
                if (result.length === 0) {
                    resolve([]);
                    return
                }
                let offers = await buildOffersData(result, lang);
                resolve(offers);
            } catch (error) {
                reject(error);
            }
        });

    }

    //TODO: Use this helper for api as well
    function attachDataToOffer(offer, offers, callback) {
        async.parallel({
            dates: Utils.asyncFromPromise(getOfferTravelDates(offer.id)),
            translations: Utils.asyncFromPromise(getOfferTranslations(offer.id)),
            photos: Utils.asyncFromPromise(getOfferPhotos(offer.id))
        }, function (err, results) {
            if (err) {
                catchError(res, err, '---offers parallel---')
            }
            offer.dates = results.dates;
            offer.translations = results.translations;
            offer.photos = results.photos;
            offers.push(offers);
            if (offers.length === result.length) {
                callback();
            }
        });
    }

    function getAllDestinations(lang, countryName) {

        const query = [
            'SELECT D.id, TR.name as name, D.country_id AS countryId FROM destination as D INNER JOIN destination_translation as TR ON TR.destination_id = D.id',
            `${countryName ? 'LEFT JOIN country as C ON C.id = D.country_id' : ''}`,
            `WHERE D.is_active = 1 AND TR.language_code = :lang ${countryName ? 'AND ' + Utils.quotedString(countryName) + ' IN (C.name_en, C.name_bg, C.name_ru)' : ''}`
        ].join(" ");

        return mysql.makeQuery(query, {
            lang: lang
        }, function (result) {
            return result;
        });
    }

    function addDestination(data) {
        const query = [
            'INSERT INTO `destination` (`country_id`, `created_on`)',
            'VALUES (:countryId, NOW());'
        ].join(" ");

        return mysql.makeQuery(query, data, function (result) {
            return result.insertId;
        });
    }

    function updateDestination(data) {
        const query = [
            'UPDATE `destination`',
            'SET country_id = :countryId WHERE id = :id;'
        ].join(" ");

        return mysql.makeQuery(query, data, function (result) {
            return result.insertId;
        });
    }

    function addDestinationTranslation(data) {
        const query = [
            'INSERT INTO  `destination_translation` (`destination_id`, `language_code`, `name`, `description`, `useful_info`)',
            'VALUES (:destinationId, :languageCode, :name, :description, :usefulInfo)',
            'ON DUPLICATE KEY UPDATE',
            'name = :name, description = :description, useful_info = :usefulInfo;'
        ].join(" ");

        return mysql.makeQuery(query, data, function (result) {
            return result.affectedRows > 0;
        });
    }

    function getDestinationData(id) {
        const query = 'SELECT D.id, D.country_id, C.name_en as countryName, TR.name, TR.language_code, TR.description, TR.useful_info FROM destination as D INNER JOIN country as C ON D.country_id = C.id INNER JOIN destination_translation as TR ON TR.destination_id = D.id WHERE D.id = :id';

        return mysql.makeQuery(query, { id }, function (result) {
            return result.length && result.length > 0 ? result : null;
        });
    }

    function deleteDestination(id) {
        const query = [
            'UPDATE destination',
            'SET is_active = 0',
            'WHERE id=:id;'
        ].join(" ");

        return mysql.makeQuery(query, { id }, function (result) {
            return result.affectedRows > 0;
        });
    }

    function getDestinationsByCountry(countryId, lang) {
        let query = "SELECT * FROM destination WHERE country_id = :countryId"
        if (lang) {
            query = 'SELECT destination.id, DT.name FROM destination INNER JOIN destination_translation as DT ON DT.destination_id = destination.id WHERE destination.country_id = :countryId AND DT.language_code = :lang';
        }

        return mysql.makeQuery(query, { countryId, lang }, function (result) {
            return result
        });
    }

    function getDestinationsByCountryName(countryName) {
        const query = `SELECT * FROM destination INNER JOIN country ON destination.country_id = country.id WHERE country.name_en = ${countryName.toLowerCase()}`;

        return mysql.makeQuery(query, null, function (result) {
            return result
        });
    }
    const destinationsQuery = [
        'SELECT D.id, DT.name, OHD.offers_id as offerId FROM destination_translation as DT',
        'INNER JOIN destination as D ON D.id = DT.destination_id',
        'INNER JOIN offer_has_destination as OHD ON OHD.destination_id = DT.destination_id',
    ]
    function getDestinationsByOfferId(offerId) {
        const query = [
            ...destinationsQuery,
            'WHERE OHD.offers_id = :offerId AND DT.language_code = "bg"',
            'ORDER BY OHD.id ASC'
        ].join(" ");

        return mysql.makeQuery(query, { offerId }, (result) => result);
    }

    function getDestinationsByListOfOfferIds(offerIdList) {
        const list = offerIdList.length > 0 ? offerIdList.join(',') : - 1;
        const query = [
            ...destinationsQuery,
            'WHERE OHD.offers_id IN (' + list + ') AND DT.language_code = "bg"',
            'ORDER BY OHD.id ASC'
        ].join(" ");

        return mysql.makeQuery(query, null, (result) => result);
    }

    function addOfferDestinations(offerId, destinations) {
        let query = [
            'INSERT INTO `offer_has_destination`',
            '(`offers_id`, `destination_id`)',
            'VALUES '
        ].join(" ");

        let values = [];
        destinations.forEach(destinationId => {
            values.push(`(${offerId}, ${destinationId})`);
        });

        query += values.join(', ') + ';';
        return mysql.makeQuery(query, null, (result) => result);
    }

    function deleteOfferDestinations(offerId) {

        const query = [
            'DELETE FROM `offer_has_destination`',
            'WHERE offers_id=:offerId;'
        ].join(" ");

        return mysql.makeQuery(query, { offerId }, (result) => {
            return result.warningCount == 0;
        });
    }

    function searchOffers(data, lang) {
        // country: req.query.country,
        // destination: req.query.destination,
        // transportType: req.query.transportType,
        // month: req.query.month
        // year: req.query.year
        // categoryId
        let query = [
            `SELECT O.id, O.ref_id as refNumber, O.offer_type as offerType, O.price, O.is_promo as isPromo, O.is_active as isActive, O.created_at as createdAt, `,
            `PHOTO.path as photo, TR.name as translationName, TR.subtitle as subtitle, TR.other_info as otherInfo, DEST_TR.name as destination, COUNTRY.name_${lang} as countryName, TRANSPORT.key, TRANSPORT.label`,
            `FROM offers as O`,
            'LEFT JOIN offer_has_destination as OHD ON OHD.offers_id = O.id',
            `LEFT JOIN offer_photos as PHOTO ON PHOTO.id = (SELECT p.id FROM offer_photos as p WHERE p.offer_id = O.id LIMIT 1)`,
            `LEFT JOIN offer_translation as TR ON TR.offer_id = O.id`,
            `LEFT JOIN destination ON destination.id = OHD.destination_id`,
            `LEFT JOIN destination_translation as DEST_TR ON DEST_TR.destination_id = destination.id`,
            `LEFT JOIN country as COUNTRY ON COUNTRY.id = destination.country_id`,
            `LEFT JOIN offer_travel_dates as DATES ON DATES.offer_id = O.id`,
            `LEFT JOIN transport_type as TRANSPORT ON TRANSPORT.id = O.transport_type_id`,
            `WHERE COUNTRY.name_${lang} ${data.country ? '= :country' : 'IS NOT NULL'}`,
            `AND DEST_TR.name ${data.destination ? '= :destination' : 'IS NOT NULL'} AND DEST_TR.language_code = '${lang}'`,
            `AND TRANSPORT.label ${data.transportType ? '= :transportType' : 'IS NOT NULL'}`,
            `AND MONTH(DATES.date_from)  ${data.month ? '= :month' : 'IS NOT NULL'}`,
            `AND YEAR(DATES.date_from)  ${data.year ? '= :year' : 'IS NOT NULL'}`,
            `AND O.subcategories_id ${data.categoryId ? '= :subcategoryId' : 'IS NOT NULL'}`,
            `AND TR.language_code = '${lang}'`,
            `AND O.is_active = 1`

        ].join(" ");

        return mysql.makeQuery(query, data, function (results) {
            let offers = [];
            results.forEach(result => {
                let offer = result;
                offer['translations'] = [{ name: offer.translationName, subtitle: offer.subtitle, other_info: offer.otherInfo }];
                offer['photos'] = [{ path: offer.photo }];
                offers.push(offer);
            });
            return offers;
        });
    }

    function getAllCountries(lang) {
        const query = `SELECT * FROM country WHERE is_active = 1`;

        return mysql.makeQuery(query, null, function (result) {

            return result.map(country => {
                return {
                    id: country.id,
                    nameBg: country.name_bg,
                    nameEn: country.name_en,
                    nameRu: country.name_ru,
                    descBg: country.desc_bg,
                    descEn: country.desc_en,
                    descRu: country.desc_ru,
                    translations: Utils.formatCountryTranslations(country),
                    imageUrl: country.image_url
                }
            })
        });
    }

    function addCountry(data) {
        const query = [
            'INSERT INTO `country` (`name_en`, `name_ru`, `name_bg`, `desc_en`, `desc_ru`, `desc_bg`, `image_url`, `created_at`)',
            'VALUES (:nameEn, :nameRu, :nameBg, :descEn, :descRu, :descBg, :imageUrl, NOW());'
        ].join(" ");

        return mysql.makeQuery(query, data, function (result) {
            return result.insertId
        });
    }

    function updateCountry(data) {
        const query = [
            'UPDATE country',
            `SET name_en = :nameEn, name_ru = :nameRu, name_bg = :nameBg, ${data.imageUrl ? 'image_url = :imageUrl,' : ''}`,
            'desc_en = :descEn, desc_bg = :descBg, desc_ru = :descRu',
            'WHERE id = :countryId'
        ].join(" ");

        return mysql.makeQuery(query, data, function (result) {
            return result.affectedRows > 0;
        });
    }

    function deleteCountryPhoto(countryId) {
        const query = [
            'UPDATE country',
            `SET image_url = NULL`,
            'WHERE id = :countryId'
        ].join(" ");

        return mysql.makeQuery(query, { countryId }, function (result) {
            return result.affectedRows > 0;
        });
    }

    function deleteSubcategoryPhoto(subcategoryId) {
        const query = [
            'UPDATE subcategory',
            `SET image_url = NULL`,
            'WHERE id = :subcategoryId'
        ].join(" ");

        return mysql.makeQuery(query, { subcategoryId }, function (result) {
            return result.affectedRows > 0;
        });
    }

    function deleteCountry(id) {
        const query = [
            'UPDATE country',
            'SET is_active = 0',
            'WHERE id=:id;'
        ].join(" ");

        return mysql.makeQuery(query, { id }, function (result) {
            return result.affectedRows > 0;
        });
    }


    function getCountryData(id) {
        const query = "SELECT id, image_url as imageUrl, name_en as nameEn, name_ru as nameRu, name_bg as nameBg, desc_ru as descRu, desc_en as descEn, desc_bg as descBg FROM country WHERE id = :id LIMIT 2"

        return mysql.makeQuery(query, { id }, function (result) {
            return result.length && result.length > 0 ? result[0] : null;
        });
    }

    function getTransportTypes() {
        const query = "SELECT * FROM transport_type;";

        return mysql.makeQuery(query, null, (result) => result);
    }

    function updateHomePageSubcategories(rows) {

        let query = [
            'INSERT INTO `home_page_subcategories` (`row_id`, `subcategory_id`)',
            'VALUES '
        ].join(" ");

        let values = [];
        rows.forEach(row => {
            values.push('(' + [row.id, row.subcategoryId].join(', ') + ')');
        });

        query += values.join(', ');
        query += ' ON DUPLICATE KEY UPDATE row_id = VALUES(row_id), subcategory_id = VALUES(subcategory_id);'

        return mysql.makeQuery(query, null, function (result) {
            return result.affectedRows > 0;
        });
    }

    function getHomePageSubcategories() {
        const query = "SELECT HPS.row_id as rowId, HPS.subcategory_id as subcategoryId, S.name_en as nameEn, S.name_bg as nameBg, S.name_ru as nameRu  FROM `home_page_subcategories` as HPS INNER JOIN `subcategories` AS S ON HPS.subcategory_id = S.id";

        return mysql.makeQuery(query, null, function (subcats) {
            let result = {};
            for (let i = 0; i < subcats.length; i++) {
                const subcat = subcats[i];
                subcat.translations = {
                    bg: {
                        name: subcat.nameBg
                    },
                    en: {
                        name: subcat.nameEn
                    },
                    ru: {
                        name: subcat.nameRu
                    }
                };
                const rowId = parseInt(subcat.rowId, 10);
                if (rowId === 1) {
                    result.subCategory1 = subcat;
                } else if (rowId === 2) {
                    result.subCategory2 = subcat;
                } else if (rowId === 3) {
                    result.subCategory3 = subcat;
                }
            }
            return result;
        });
    }

    function executeSliderOffersQuery() {
        let query = 'SELECT * FROM offers WHERE level_slider > 0 ORDER BY level_slider DESC';

        return mysql.makeQuery(query, null, function (result) {
            return result.map(formatOffer);
        });
    }

    async function getSliderOffers(lang) {

        return new Promise(async function (resolve, reject) {
            try {
                let result = await executeSliderOffersQuery();
                let offers = await buildOffersData(result, lang);
                resolve(offers);
            } catch (error) {
                reject(error);
            }
        });

    }

    return {
        getTransportTypes,
        getAllSubcategories,
        addSubcategory,
        updateSubcategory,
        deleteSubcategory,
        addOfferTranslations,
        addOfferDestinations,
        deleteOfferDestinations,
        addOfferTravelDates,
        addOffer,
        getOffer,
        getOffers: getOffers,
        getOfferTranslations,
        getOfferTravelDates,
        getTravelDatesByListOfOfferIds,
        deleteOfferTravelDates,
        deleteOfferTranslations,
        updateOffer: updateOffer,
        getHomePageData: getHomePageData,
        getSubcategoryById,
        deleteAllOffersBySubcategoryId,
        addOfferPhotos: addOfferPhotos,
        deleteOfferPhotos: deleteOfferPhotos,
        getOfferPhotos: getOfferPhotos,
        deleteOffer: deleteOffer,
        getOffersBySubcategory,
        getOffersByCountry,
        getAllDestinations,
        addPhotos,
        getPhotos,
        addDestination,
        getDestinationData,
        getDestinationsByCountry,
        searchOffers: searchOffers,
        getAllCountries: getAllCountries,
        addCountry: addCountry,
        updateCountry,
        deleteCountry,
        deleteCountryPhoto,
        addDestinationTranslation,
        getCountryData: getCountryData,
        deleteDestination,
        updateDestination,
        deletePhoto: deletePhoto,
        updateHomePageSubcategories,
        getDestinationsByOfferId,
        getDestinationsByListOfOfferIds,
        getHomePageSubcategories,
        getAllCategories,
        getDestinationsByCountryName,
        getSliderOffers,
        getOffersByDestination,
        deleteSubcategoryPhoto
    };
}