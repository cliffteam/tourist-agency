const Utils = require('../../../lib/utils');
const async = require('async');
const HotelService = new (require('../../hotel/HotelService'))();
const appConfig = require('../../../config/main.config');
const FileService = new (require('../../../lib/FileService'))();

const DEFAULT_LANGUAGE = 'bg';

module.exports = function OfferController(OfferRepository) {

    function getAllCategories(req, res) {
        OfferRepository.getAllCategories().then(function (result) {
            res.json(result);
        }).catch(function (err) {
            catchError(res, err, '---getAllCategories---')
        });
    }

    function getAllSubcategories(req, res) {
        OfferRepository.getAllSubcategories().then(function (result) {
            res.json(result);
        }).catch(function (err) {
            catchError(res, err, '---getAllCategories---')
        });
    }

    function addSubcategory(req, res) {
        let subcategory = JSON.parse(req.body.subcategory);
        if (!(req.files.photo && req.files.photo.length > 0)) {
            catchError(res, "Добавете снимка");
            return;
        }
        const data = {
            nameEn: subcategory.nameEn,
            nameBg: subcategory.nameBg,
            nameRu: subcategory.nameRu,
            categoryId: subcategory.categoryId,
            imagePath: req.files.photo[0].filename,
        }
        OfferRepository.addSubcategory(data).then(function (result) {
            res.json({ success: result.id ? true : false, result: result });
        }).catch(function (err) {
            catchError(res, err, '---addCategory---')
        });
    }

    function updateSubcategory(req, res) {
        let subcategory = JSON.parse(req.body.subcategory);
        var data = {
            nameEn: subcategory.nameEn,
            nameBg: subcategory.nameBg,
            nameRu: subcategory.nameRu,
            categoryId: subcategory.categoryId
        }
        if (req.files.photo && req.files.photo.length > 0) {
            data.imageUrl = req.files.photo[0].filename;
        }
        OfferRepository.updateSubcategory(req.params.id, data).then(function (success) {
            res.json({ success: success });
        }).catch(function (err) {
            catchError(res, err, '---updateSubcategory---')
        });
    }

    function deleteSubcategoryPhoto(req, res) {
        OfferRepository.deleteSubcategoryPhoto(req.params.subcategoryId).then(success => {
            res.json({ success: success });
        }).catch(function (err) {
            catchError(res, err, '---deleteSubcategoryPhoto---')
        });
    }

    function deleteSubcategory(req, res) {
        OfferRepository.getOffers(req.params.id).then(result => {
            if (result.length > 0) {
                res.status(400).json({
                    success: false,
                    msg: 'Cannot delete non-empty subcategory.',
                    result: result
                });
            } else {
                OfferRepository.deleteSubcategory(req.params.id).then(function (success) {
                    res.json({ success: success });
                }).catch(function (err) {
                    catchError(res, err, '---deletesubcategory---')
                });
            }
        }).catch(function (err) {
            catchError(res, err, '---deletesubcategory---')
        });;
    }

    function getAllOffers(req, res) {
        const subcategory = req.query.subcat;
        let offers = [];
        OfferRepository.getOffers(subcategory).then(function (result) {
            if (result.length && result.length > 0) {
                result.forEach(el => {
                    async.parallel({
                        dates: Utils.asyncFromPromise(OfferRepository.getOfferTravelDates(el.id)),
                        translations: Utils.asyncFromPromise(OfferRepository.getOfferTranslations(el.id)),
                        photos: Utils.asyncFromPromise(OfferRepository.getOfferPhotos(el.id)),
                        destinations: Utils.asyncFromPromise(OfferRepository.getOfferPhotos(el.id))
                    }, function (err, results) {
                        if (err) {
                            catchError(res, err, '---offers parallel---')
                        }
                        el.dates = results.dates;
                        el.translations = results.translations;
                        el.photos = results.photos;
                        offers.push(el);
                        if (offers.length === result.length) {
                            res.json({ success: true, offers: offers });
                        }
                    });
                });
            } else {
                res.json({ success: false, msg: 'No offers in the database' });
            }
        }).catch(function (err) {
            catchError(res, err, '---getOffers---')
        });
    }

    function getOfferData(req, res) {
        OfferRepository.getOffer(req.params.id).then(function (offerResult) {
            if (offerResult) {
                async.parallel({
                    dates: Utils.asyncFromPromise(OfferRepository.getOfferTravelDates(req.params.id)),
                    translations: Utils.asyncFromPromise(OfferRepository.getOfferTranslations(req.params.id)),
                    photos: Utils.asyncFromPromise(OfferRepository.getOfferPhotos(req.params.id)),
                    hotels: Utils.asyncFromPromise(HotelService.getHotelsByOfferId(req.params.id)),
                    destinations: Utils.asyncFromPromise(OfferRepository.getDestinationsByOfferId(req.params.id)),
                }, function (err, results) {
                    let offer = offerResult;

                    if (err) {
                        catchError(res, err, '---offers parallel---')
                    }
                    offer.hotels = results.hotels;
                    offer.dates = results.dates;
                    offer.translations = results.translations;
                    offer.destinations = results.destinations.map(x => x.id);

                    offer['photos'] = FileService.formatPhotos(req, results.photos, appConfig.PUBLIC_PATH_OFFERS);
                    res.json({
                        success: true,
                        offer: offer
                    });
                });
            } else {
                res.json({ success: false, msg: 'No offers in the database' });
            }
        }).catch(function (err) {
            catchError(res, err, '---getOffers---')
        });
    }

    function addOffer(req, res) {
        let offerData = JSON.parse(req.body.offer);

        if (!(offerData.subcategoryId &&
            offerData.dates.length > 0 &&
            offerData.destinations.length > 0 &&
            offerData.transportType
        )) {
            catchError(res, "Моля попълнете всички задължителни (*) полета");
            return;
        }

        if (!(offerData.photos.length > 0)) {
            catchError(res, "Моля, добавете снимки");
            return;
        }

        for (let i = 0; i < offerData.translations.length; i++) {
            const tr = offerData.translations[i];
            if (tr.languageCode === "bg") {
                let bg = tr;
                if (!(
                    bg.itinerary &&
                    bg.duration &&
                    bg.name &&
                    bg.otherInfo &&
                    bg.schedule
                )) {
                    catchError(res, "Моля, попълнете всички полета на български език");
                    return;
                }
            }
        }

        OfferRepository.addOffer(offerData).then(function (result) {
            if (result.id) {

                const dates = offerData.dates;
                const translations = offerData.translations;
                const destinations = offerData.destinations;
                const hotelsIdList = offerData.hotels.map(hotel => {
                    return {
                        hotelId: hotel.id,
                        offerId: result.id,
                    };
                });

                const photoFiles = req.files['photos'] ? req.files['photos'].map(f => f.filename) : [];
                const photos = [];
                for (let i = 0; i < photoFiles.length; i++) {
                    const file = photoFiles[i];
                    photos.push({ file });
                }
                const parallel = {
                    travelDates: Utils.asyncFromPromise(OfferRepository.addOfferTravelDates(result.id, dates)),
                    translations: Utils.asyncFromPromise(OfferRepository.addOfferTranslations(result.id, translations)),
                    destinations: Utils.asyncFromPromise(OfferRepository.addOfferDestinations(result.id, destinations)),
                };

                if (photos.length > 0) {
                    parallel.photos = Utils.asyncFromPromise(OfferRepository.addOfferPhotos(result.id, photos));
                }

                if (hotelsIdList.length > 0) {
                    parallel.hotels = Utils.asyncFromPromise(HotelService.addHotelsToOfferId(hotelsIdList));
                }

                async.parallel(parallel, function (err, results) {
                    if (err) {
                        catchError(res, err, '---addOfferDatesAndTranslations---')
                        return;
                    }
                    res.json({ success: true, offerId: result.id });
                });
            }
        }).catch(function (err) {
            catchError(res, err, '---addOffer---')
        });
    }

    function updateOffer(req, res) {
        const offerData = JSON.parse(req.body.offer);
        const offerId = req.params.id;

        async.parallel({
            deleteDates: Utils.asyncFromPromise(OfferRepository.deleteOfferTravelDates(offerId)),
            deleteTranslations: Utils.asyncFromPromise(OfferRepository.deleteOfferTranslations(offerId)),
            deleteDestinations: Utils.asyncFromPromise(OfferRepository.deleteOfferDestinations(offerId)),
            deleteHotels: Utils.asyncFromPromise(HotelService.removeHotelsByOfferId(offerId))

        }, function (err, results) {

            if (err) {
                catchError(res, err, '---deleteOfferDatesAndTranslations---')
            }

            if (results.deleteDates && results.deleteTranslations) {

                const dates = offerData.dates;
                const translations = offerData.translations;
                const destinations = offerData.destinations;
                const hotelsIdList = offerData.hotels.map(hotel => {
                    return {
                        hotelId: hotel.id,
                        offerId: offerId,
                    };
                });
                const photoFiles = req.files['photos'] ? req.files['photos'].map(f => f.filename) : [];
                const photos = [];
                for (let i = 0; i < photoFiles.length; i++) {
                    const file = photoFiles[i];
                    photos.push({ file, isHead: offerData.photos[i].isHead });
                }
                const parallel = {
                    updateOffer: Utils.asyncFromPromise(OfferRepository.updateOffer(offerId, offerData)),
                    travelDates: Utils.asyncFromPromise(OfferRepository.addOfferTravelDates(offerId, dates)),
                    translations: Utils.asyncFromPromise(OfferRepository.addOfferTranslations(req.params.id, translations)),
                    destinations: Utils.asyncFromPromise(OfferRepository.addOfferDestinations(req.params.id, destinations)),
                };

                if (photos.length > 0) {
                    parallel.photos = Utils.asyncFromPromise(OfferRepository.addOfferPhotos(offerId, photos));
                }

                if (hotelsIdList.length > 0) {
                    parallel.hotels = Utils.asyncFromPromise(HotelService.addHotelsToOfferId(hotelsIdList));
                }

                async.parallel(parallel, function (err, updResults) {
                    if (err) {
                        console.log('eerr', err);
                        catchError(res, err, '---updateOfferDatesAndTranslations---')
                    }
                    res.json({
                        success: true,
                        results: updResults
                    });
                }).catch(function (err) {
                    catchError(res, err, '---updateOfferDatesAndTranslations-async---')
                });
            }
        });
    }

    function deleteOffer(req, res) {
        OfferRepository.deleteOffer(req.params.id).then(function (success) {
            res.json({ success: success });
        }).catch(function (err) {
            catchError(res, err, '---deleteOffer---')
        });
    }

    function deleteOfferPhoto(req, res) {
        OfferRepository.deletePhoto("offer", req.params.id).then(success => {
            res.json({ success: success });
        }).catch(function (err) {
            catchError(res, err, '---deleteOfferPhoto---')
        });
    }

    function deleteDestinationPhoto(req, res) {
        OfferRepository.deletePhoto("destination", req.params.id).then(success => {
            res.json({ success: success });
        }).catch(function (err) {
            catchError(res, err, '---deleteDestinationPhoto---')
        });
    }

    function getSubcategoryById(req, res) {
        OfferRepository.getSubcategoryById(req.params.id).then(data => {

            if (data.imageUrl) {
              const formatted = FileService.formatPhotosFromName(req, [data.imageUrl], appConfig.PUBLIC_PATH_CATEGORIES)[0];
              data['imageUrl'] = formatted;
            } else {
              data['imageUrl'] = null;
            }
            res.json(data);
        }).catch(function (err) {
            catchError(res, err, '---getsubcategoryById---')
        });
    }

    async function getAllDestinations(req, res) {
        try {
            let destinations = await OfferRepository.getAllDestinations(req.lang || DEFAULT_LANGUAGE);
            res.json({
                destinations
            });
        } catch (err) {
            catchError(res, err);
        }
    }

    async function getOffersByDestination(req, res) {
        try {
            let offers = await OfferRepository.getOffersByDestination();
            res.json({
                offers: offers
            });
        } catch (err) {
            catchError(res, err);
        }
    }

    async function updateDestination(req, res) {
        try {

            const destData = JSON.parse(req.body.destination);

            if (!destData.photos || !destData.photos.length === 0) {
                catchError(res, "Изберете снимка");
                return;
            }
            const destinationId = req.params.id;
            await OfferRepository.updateDestination({ countryId: destData.countryId, id: destinationId });

            for (let i = 0; i < destData.translations.length; i++) {
                const translation = destData.translations[i];
                let data = {
                    id: translation.id,
                    destinationId: destinationId,
                    languageCode: translation.languageCode,
                    name: translation.name,
                    description: translation.description,
                    usefulInfo: translation.usefulInfo
                }
                await OfferRepository.addDestinationTranslation(data);
            }

            // TODO: photos will be deleted by a separate query so we should just add the new ones here
            if (req.files.photos && req.files.photos.length > 0) {
                const photos = req.files.photos.map(photo => photo.filename);
                await OfferRepository.addPhotos('destination', destinationId, photos);
            }

            res.json({
                success: true
            });

        } catch (err) {
            catchError(res, err);
        }
    }

    async function addDestination(req, res) {
        try {

            var destData = JSON.parse(req.body.destination);

            if (!destData.countryId) {
                catchError(res, "Изберете държава");
                return;
            }

            if (!(req.files.photos && req.files.photos.length > 0)) {
                catchError(res, "Изберете снимка");
                return;
            }

            let destId = await OfferRepository.addDestination({ countryId: destData.countryId });
            if (!destId) {
                catchError(res, "Добавянето на дестинация е неуспешно!");
                return;
            }

            destData.translations.forEach(async translation => {
                let data = {
                    destinationId: destId,
                    languageCode: translation.languageCode,
                    name: translation.name,
                    description: translation.description,
                    usefulInfo: translation.usefulInfo
                }
                // TODO: add to repository
                await OfferRepository.addDestinationTranslation(data);
            });

            const photos = req.files['photos'] ? req.files['photos'].map(f => f.filename) : [];
            let success = await OfferRepository.addPhotos('destination', destId, photos);

            res.json({ success });

        } catch (err) {
            catchError(res, err);
        }
    }

    async function getDestinationData(req, res) {
        try {
            let destination = await OfferRepository.getDestinationData(req.params.id);
            if (!destination) {
                catchError(res, "Не е намерена такава дестинация");
                return;
            }

            let photos = await OfferRepository.getPhotos('destination', destination[0].id);

            let result = {
                id: destination[0].id,
                country: {
                    id: destination[0].country_id,
                    name: destination[0].countryName
                },
                translations: [],
                countryId: destination[0].country_id,
                photos: FileService.formatPhotos(req, photos, appConfig.PUBLIC_PATH_DESTINATIONS)
            }
            destination.forEach(dest => {
                result.translations.push({
                    name: dest.name,
                    languageCode: dest.language_code,
                    description: dest.description,
                    usefulInfo: dest.useful_info
                })
            });

            res.json({
                destination: result
            });
        } catch (err) {
            catchError(res, err);
        }
    }

    async function getDestinationsByCountry(req, res) {
        try {
            let destinations = await OfferRepository.getDestinationsByCountry(countryId);
            res.json({ destinations });
        } catch (err) {
            catchError(res, err);
        }
    }

    async function deleteDestination(req, res) {
        try {
            const result = await OfferRepository.deleteDestination(req.params.id);
            res.json({ success: result });
        } catch (err) {
            catchError(res, err);
        }
    }

    async function searchOffers(req, res) {

        let searchData = {
            country: req.query.country,
            destination: req.query.destination,
            transportType: req.query.transportType,
            month: req.query.month,
            year: req.query.year,
            subcategoryId: req.query.subcategoryId
        }

        try {
            let offers = await OfferRepository.searchOffers(searchData, req.lang || "en");
            res.json({ offers });
        } catch (err) {
            catchError(res, err);
        }
    }

    async function getAllCountries(req, res) {
        try {
            const result = await OfferRepository.getAllCountries(req.lang || "en");
            res.json({ result });
        } catch (err) {
            catchError(res, err);
        }
    }

    async function addCountry(req, res) {
        try {
            const data = JSON.parse(req.body.country);
            if (!(req.files.photo && req.files.photo.length > 0)) {
                catchError(res, "Добавете снимка");
                return;
            }
            const country = {
                nameBg: data.nameBg,
                nameEn: data.nameEn,
                nameRu: data.nameRu,
                imageUrl: req.files.photo[0].filename,
                descBg: data.descBg,
                descEn: data.descEn,
                descRu: data.descRu
            };

            const countryId = await OfferRepository.addCountry(country)
            res.json({
                countryId: countryId,
                success: countryId > 0
            });
        } catch (err) {
            catchError(res, err);
        }
    }

    async function updateCountry(req, res) {
        try {

            const data = JSON.parse(req.body.country);
            const country = {
                countryId: data.id,
                nameBg: data.nameBg,
                nameEn: data.nameEn,
                nameRu: data.nameRu,
                descBg: data.descBg,
                descEn: data.descEn,
                descRu: data.descRu
            }
            if (req.files.photo && req.files.photo.length > 0) {
                country.imageUrl = req.files.photo[0].filename;
            }
            const result = await OfferRepository.updateCountry(country);
            res.json({ success: result });
        } catch (err) {
            catchError(res, err);
        }
    }

    async function deleteCountry(req, res) {
        try {
            const result = await OfferRepository.deleteCountry(req.params.id);
            res.json({ success: result });
        } catch (err) {
            catchError(res, err);
        }
    }

    function deleteCountryPhoto(req, res) {
        OfferRepository.deleteCountryPhoto(req.params.countryId).then(success => {
            res.json({ success: success });
        }).catch(function (err) {
            catchError(res, err, '---deleteCountryPhoto---')
        });
    }

    async function getCountryData(req, res) {
        try {
            const result = await OfferRepository.getCountryData(req.params.id);
            if (result.imageUrl) {
                const formatted = FileService.formatPhotosFromName(req, [result.imageUrl], appConfig.PUBLIC_PATH_COUNTRIES)[0];
                result['imageUrl'] = formatted;
            } else {
                result['imageUrl'] = null;
            }
            res.json({ country: result });
        } catch (err) {
            catchError(res, err);
        }
    }
    async function getTransportTypes(req, res) {
        try {
            const transportTypes = await OfferRepository.getTransportTypes();
            res.json({ transportTypes: transportTypes });
        } catch (err) {
            catchError(res, err);
        }
    }

    async function updateHomePageSubcategories(req, res) {
        try {
            const rows = [
                { id: 1, subcategoryId: req.body.subCategory1 },
                { id: 2, subcategoryId: req.body.subCategory2 },
                { id: 3, subcategoryId: req.body.subCategory3 }
            ];
            const success = await OfferRepository.updateHomePageSubcategories(rows);
            res.json({ success });
        } catch (err) {
            catchError(res, err);
        }
    }

    async function getHomePageSubcategories(req, res) {
        try {
            const subcats = await OfferRepository.getHomePageSubcategories();

            res.json({ succes: true, subcategories: subcats });
        } catch (err) {
            catchError(res, err);
        }
    }

    return {
        getTransportTypes,
        addSubcategory,
        getAllSubcategories,
        updateSubcategory,
        deleteSubcategory,
        getAllOffers,
        addOffer,
        updateOffer,
        getSubcategoryById,
        deleteOfferPhoto,
        deleteOffer,
        getOfferData,
        getAllDestinations,
        getOffersByDestination,
        addDestination,
        getDestinationData,
        searchOffers,
        getDestinationsByCountry,
        getAllCountries,
        addCountry,
        getCountryData,
        updateCountry,
        deleteCountry,
        deleteCountryPhoto,
        deleteDestination,
        updateDestination,
        deleteDestinationPhoto,
        updateHomePageSubcategories,
        getHomePageSubcategories,
        getAllCategories,
        deleteSubcategoryPhoto
    };
};