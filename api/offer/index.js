var mysql = require('../../lib/mysqlConnection/MysqlConnection'),
	config = require('../../config/config');

module.exports = function () {

	var OfferRepository = new(require('./repositories/OfferRepository'))(mysql),
		OfferController = new (require('./controllers/OfferController'))(OfferRepository);

	return require ('./routes/OfferRoutes')(OfferController);
};