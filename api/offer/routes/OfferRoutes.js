const express = require('express');
const router = express.Router();
const jwtauth = require('../../user/jwtAuth.js');
const multer = require('multer');
const appConfig = require('../../../config/main.config');
const fileService = new (require('../../../lib/FileService'))();

const categoriesPhotosStorage = fileService.createStorage(multer, appConfig.UPLOADS_PATH_CATEGORIES);
const offersPhotosStorage = fileService.createStorage(multer, appConfig.UPLOADS_PATH_OFFERS);
const countriesPhotosStorage = fileService.createStorage(multer, appConfig.UPLOADS_PATH_COUNTRIES);
const destinationsPhotosStorage = fileService.createStorage(multer, appConfig.UPLOADS_PATH_DESTINATIONS);

const uploadCategories = multer({ storage: categoriesPhotosStorage });
const uploadOffers = multer({ storage: offersPhotosStorage });
const uploadCountries = multer({ storage: countriesPhotosStorage });
const uploadDestinations = multer({ storage: destinationsPhotosStorage });

const catUpload = uploadCategories.fields([{ name: 'photo', maxCount: 1 }]);
const offerUpload = uploadOffers.fields([{ name: 'photos', maxCount: 20 }]);
const countriesUpload = uploadCountries.fields([{ name: 'photo', maxCount: 1 }]);
const destinationsUpload = uploadDestinations.fields([{ name: 'photos', maxCount: 20 }]);

module.exports = function (OfferController) {
  router.get('/transport-type', jwtauth, OfferController.getTransportTypes);

  router.post('/subcategories/new', jwtauth, catUpload, OfferController.addSubcategory);
  router.get('/categories', jwtauth, OfferController.getAllCategories);
  router.get('/subcategories', jwtauth, OfferController.getAllSubcategories);
  router.get('/subcategories/home-page', OfferController.getHomePageSubcategories);
  router.put('/subcategories/home-page', OfferController.updateHomePageSubcategories);
  router.get('/subcategories/:id', jwtauth, catUpload, OfferController.getSubcategoryById);
  router.put('/subcategories/:id', jwtauth, catUpload, OfferController.updateSubcategory);
  router.delete('/subcategories/photos/:subcategoryId', jwtauth, OfferController.deleteSubcategoryPhoto);
  router.delete('/subcategories/:id', jwtauth, OfferController.deleteSubcategory);

  router.get('/', jwtauth, OfferController.getAllOffers);
  router.post('/new', jwtauth, offerUpload, OfferController.addOffer);
  router.put('/:id', jwtauth, offerUpload, OfferController.updateOffer);
  router.delete('/:id', jwtauth, OfferController.deleteOffer);
  router.delete('/photos/:id', jwtauth, OfferController.deleteOfferPhoto);

  router.get('/destinations', OfferController.getAllDestinations);
  router.post('/destinations', jwtauth, destinationsUpload, OfferController.addDestination);
  router.get('/destinations/:id', jwtauth, OfferController.getDestinationData);
  router.put('/destinations/:id', jwtauth, destinationsUpload, OfferController.updateDestination);
  router.delete('/destinations/:id', jwtauth, OfferController.deleteDestination);
  router.delete('/destinations/photos/:id', jwtauth, OfferController.deleteDestinationPhoto);

  router.get('/countries', OfferController.getAllCountries);
  router.get('/countries/:id', OfferController.getCountryData);
  router.put('/countries/:id', jwtauth, countriesUpload, OfferController.updateCountry);
  router.delete('/countries/photos/:countryId', jwtauth, OfferController.deleteCountryPhoto);
  router.delete('/countries/:id', jwtauth, OfferController.deleteCountry);
  router.post('/countries', jwtauth, countriesUpload, OfferController.addCountry);

  router.get('/:id', jwtauth, OfferController.getOfferData);
  router.get('/:dest', OfferController.getOffersByDestination);

  return router;
};
