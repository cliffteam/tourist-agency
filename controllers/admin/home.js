
var moment = require('moment');
var multer = require('multer');
var crypto = require('crypto');
var nodemailer = require('nodemailer');



var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/uploads')
    },
    filename: function (req, file, cb) {
        var hashedString = crypto.createHash('md5').update(new Date().toString()).digest('hex');
        var extensionArray = file.mimetype.split('/');
        var extension = extensionArray[1];
        cb(null, hashedString + '.' + extension);
    }
});

var upload = multer({ storage: storage }).any();
exports.index = (req, res) => {
    res.render('admin/index', {
        title: 'Home'
    });
};
exports.uploadFile = (req, res, next) => {
    upload(req, res, function (err) {
        console.log(req.files[0]);
        if (err) {
            console.log(err);
            return
        }
        if (!req.files[0]) {
            res.json({
                success: false
            });
            return;
        }
        res.json({
            success: true,
            filename: req.files[0].filename,
            filepath: req.files[0].path
        });
    });
};
/*exports.contacts = (req, res) => {

    AdminRepository.isEmailAdded(req.body.email).then((isAdded) =>{
        if (isAdded === false) {
            AdminRepository.addEmailToEmailList(req.body.email).then(() => {
            }).catch(function(err){
                console.error('addEmailToEmailList', err);
            });
        }
    }).catch(function(err){
        console.error('isEmailAdded', err);
    });
    var smtpConfig = {
        host: 'giga.superhosting.bg',
        requireTLS: true, //Force TLS
        port: 465,
        secure: true, // use SSL 
        auth: {
            user: '',
            pass: ''
        }
    };
    var transporter = nodemailer.createTransport(smtpConfig);
     
    // setup e-mail data with unicode symbols 
    var mailOptions = {
        from: req.body.email, // sender address 
        to: 'martin@mslworkshop.com', // list of receivers 
        subject: 'Hello ✔', // Subject line 
        text: 'Hello world 🐴', // plaintext body 
        html: '<b>Hello world 🐴</b>' // html body 
    };
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            res.json({
                success: false
            });
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
        res.json({
            success: true
        });
    });
};*/

return exports;