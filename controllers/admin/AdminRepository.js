'use strict'
const mysql = require('../../lib/mysqlConnection/MysqlConnection');

module.exports = function AdminRepository() {
    function addEmailToEmailList(email) {
        const query = [
            'INSERT INTO newsletter',
            '(`email`)',
            'VALUES (:email);'
        ].join(" ");

        return mysql.makeQuery(query, {
            email: email
        }, (result) => result.insertId);
    }
    function isEmailAdded(email) {
        const query = [
            'SELECT COUNT(*) AS number FROM newsletter',
            'WHERE email = :email;'
        ].join(" ");

        return mysql.makeQuery(query, {
            email: email
        }, (result) => result[0] ? result[0].number > 0 : false);
    }
    return {
        addEmailToEmailList,
        isEmailAdded
    };
};