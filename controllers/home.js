'use strict'
/**
 * GET /
 * Home page.
 */
const mysql = require('../lib/mysqlConnection/MysqlConnection'),
    config = require('../config/config'),
    moment = require('moment');
let i18n = require('i18n-express');
let async = require('async');
let Utils = require('../lib/utils');
const FileService = new (require('../lib/FileService'))();
const appConfig = require('../config/main.config');

let OfferRepository = require('../api/offer/repositories/OfferRepository')(mysql, config);
let HotelService = require('../api/hotel/HotelService')(mysql, config);
let HotelRepository = require('../api/hotel/HotelRepository')(mysql, config);
const AdminRepository = require('./admin/AdminRepository')();

const ResultType = {
    DESTINATIONS: 1,
    CALENDAR: 2,
    SEARCH: 3,
    VACATIONS: 4
}


const transportTypesToId = {
    bus: 1,
    plane: 2,
    cruise: 3,
    multiple: 4,
    own: 5
}
const TransportTypeToTranslationKey = {
    1: 'bus',
    2: 'plane',
    3: 'cruise',
    4: 'multple',
    5: 'own'
}
const TransportType = {
    1: 'Автобус',
    2: 'Самолет',
    3: 'Круиз',
    4: 'Комбиниран',
    5: 'Собствен'
};

const TransportTypeTranslations = {
    en: ["Bus", "Plane", "Cruise", "Combined", "Own"],
    bg: ["Автобус", "Самолет", "Круиз", "Комбиниран", "Собствен"],
    ru: ["Автобус", "Самолет", "Круиз", "Комбинированный", "Собственный"]
};

const MonthsTranslations = {
    bg: [{ name: "Януари", id: 1 }, { name: "Февруари", id: 2 }, { name: "Март", id: 3 }, { name: "Април", id: 4 }, { name: "Май", id: 5 }, { name: "Юни", id: 6 }, { name: "Юли", id: 7 }, { name: "Август", id: 8 }, { name: "Септември", id: 9 }, { name: "Октомври", id: 10 }, { name: "Ноември", id: 11 }, { name: "Декември", id: 12 }],
    en: [{ name: "January", id: 1 }, { name: "February", id: 2 }, { name: "March", id: 3 }, { name: "April", id: 4 }, { name: "May", id: 5 }, { name: "June", id: 6 }, { name: "July", id: 7 }, { name: "August", id: 8 }, { name: "September", id: 9 }, { name: "October", id: 10 }, { name: "November", id: 11 }, { name: "December", id: 12 }],
    ru: [{ name: "Январь", id: 1 }, { name: "Февраль", id: 2 }, { name: "Март", id: 3 }, { name: "Апрель", id: 4 }, { name: "Май", id: 5 }, { name: "Июнь", id: 6 }, { name: "Июль", id: 7 }, { name: "Август", id: 8 }, { name: "Сентябрь", id: 9 }, { name: "Октябрь", id: 10 }, { name: "Ноябрь", id: 11 }, { name: "Декабрь", id: 12 }]
};

function getNext12MonthsWithYear(lang) {
    const months = [];
    const tmpDate = new Date();
    let tmpYear = tmpDate.getFullYear();
    let tmpMonth = tmpDate.getMonth();
    let monthLiteral;

    for (let i = 0; i < 12; i++) {
        tmpDate.setMonth(tmpMonth + i);
        tmpDate.setFullYear(tmpYear);
        monthLiteral = MonthsTranslations[lang][tmpMonth].name;
        months.push({
            month: tmpMonth + 1,
            year: tmpYear,
            literal: `${monthLiteral} ${tmpYear}`
        });
        tmpMonth = (tmpMonth == 11) ? 0 : tmpMonth + 1;
        tmpYear = (tmpMonth == 11) ? tmpYear + 1 : tmpYear;
    }

    return months;
}

function getNext12MonthsWithYear(lang) {
    var months = new Array();
    var today = new Date();
    var tmpDate = new Date();
    var tmpYear = tmpDate.getFullYear();
    var tmpMonth = tmpDate.getMonth();
    var monthLiteral;

    for (var i = 0 ; i < 12 ; i++) {
       tmpDate.setMonth(tmpMonth + i);
       tmpDate.setFullYear(tmpYear);
       monthLiteral = MonthsTranslations[lang][tmpMonth].name;
       months.push({month: tmpMonth + 1, year: tmpYear, literal: monthLiteral + ' ' + tmpYear});
       tmpMonth = (tmpMonth == 11) ? 0 : tmpMonth+1;
       tmpYear = (tmpMonth == 11) ? tmpYear+1 : tmpYear;
    }

    return months;
  }

module.exports = function HomeController() {
    const defaultQueries = {
        categories: Utils.asyncFromPromise(OfferRepository.getAllCategories()),
        subcategories: Utils.asyncFromPromise(OfferRepository.getAllSubcategories()),

    };
    async function getHomePageOffers(subcategories, lang) {

        if (!subcategories || subcategories.length === 0) {
            console.log("No subcategories selected");
            return
        }
        let request = {};
        for (const key in subcategories) {
            if (subcategories.hasOwnProperty(key)) {
                const subcat = subcategories[key].subcategoryId;
                // subcategories[key.toString()] = { offers : []};
                subcategories[key.toString()].offers = await OfferRepository.getOffersBySubcategory(subcat, lang);
            }
        }
        return subcategories
    }


    let index = async (req, res) => {
        const selectedData = Utils.fetchSelectedData(req);
        const lang = Utils.getCurrentLang(req);
        let sliderOffers = await OfferRepository.getSliderOffers(lang);
        const homePageSubcategories = await OfferRepository.getHomePageSubcategories();
        const homePageOffers = await getHomePageOffers(homePageSubcategories, lang);
        let offerIdsList = Utils.extractOfferIdsFromHomeCategories(homePageOffers);


        sliderOffers.map(offer => {
            offerIdsList.push(offer.id);
            const photos = offer.photos.map(photo => photo.path);
            offer.photos = FileService.formatPhotosFromName(req, photos, appConfig.PUBLIC_PATH_OFFERS);
        });

        offerIdsList = Utils.uniqueValues(offerIdsList);

        let requests = {
            ...defaultQueries,
            countries: Utils.asyncFromPromise(OfferRepository.getAllCountries(lang)),
            destinations: Utils.asyncFromPromise(OfferRepository.getAllDestinations(lang, selectedData.country)),
            offerDestinations: Utils.asyncFromPromise(OfferRepository.getDestinationsByListOfOfferIds(offerIdsList)),
            dates: Utils.asyncFromPromise(OfferRepository.getTravelDatesByListOfOfferIds(offerIdsList))
        };

        /*if (selectedData.country) {
            requests['destinations'] = Utils.asyncFromPromise(OfferRepository.getDestinationsByCountry(selectedData.country))
        }*/

        async.parallel(requests, async function (err, results) {

            Object.keys(homePageOffers).forEach(key => {
                homePageSubcategories[key].offers = _formatOffersForCardItem(req, homePageSubcategories[key].offers, results.offerDestinations, results.dates);
            });

            Utils.sortCountryByNameAsc(lang, results.countries);
            res.render('public/home', {
                title: 'Home',
                path: '',
                categories: Utils.getCategoriesWithSubcategories(results.categories, results.subcategories) || [],
                destinationsBlock: results.countries,
                homePageSubcategories: homePageOffers,
                countries: results.countries,
                destinations: results.destinations,
                months: getNext12MonthsWithYear(lang),
                sliderOffers,
                transportTypes: TransportTypeTranslations[lang],
                selectedData: selectedData, // TODO: check for XSS
                lang,
                // toCamel: Utils.toCamel // TODO: reconsider this (currently used because we get nameEn, nameRu etc)
            });
        });
    };

    let searchResults = async (req, res) => {
        const lang = Utils.getCurrentLang(req);

        const selectedData = Utils.fetchSelectedData(req);
        let offers = await OfferRepository.searchOffers(selectedData, lang);
        const offerIdsList = offers.map(offer => offer.id);

        async.parallel({
            categories: Utils.asyncFromPromise(OfferRepository.getAllCategories()),
            subcategories: Utils.asyncFromPromise(OfferRepository.getAllSubcategories()),
            offerDestinations: Utils.asyncFromPromise(OfferRepository.getDestinationsByListOfOfferIds(offerIdsList)),
            dates: Utils.asyncFromPromise(OfferRepository.getTravelDatesByListOfOfferIds(offerIdsList)),
        }, function (err, results) {
            if (err) {
                console.log(err);

            }
            if (offers.length > 0) {
                offers = _formatOffersForCardItem(req, offers, results.offerDestinations, results.dates);
            }
            res.render('public/results', {
                title: 'Search results',
                path: null,
                categories: Utils.getCategoriesWithSubcategories(results.categories, results.subcategories),
                offers: offers,
                type: ResultType.SEARCH // this is just an example
            });
        });

    };

    let offersBySubcategory = async (req, res) => {
        const lang = Utils.getCurrentLang(req)
        const subcategoryId = parseInt(req.params.id, 10);
        const selectedData = Utils.fetchSelectedData(req);

        let offers = await OfferRepository.getOffersBySubcategory(subcategoryId, lang);
        const offerIdsList = offers.map(offer => offer.id);
        async.parallel({
            ...defaultQueries,
            countries: Utils.asyncFromPromise(OfferRepository.getAllCountries(lang)),
            destinations: Utils.asyncFromPromise(OfferRepository.getAllDestinations(lang, selectedData.country)),
            offerDestinations: Utils.asyncFromPromise(OfferRepository.getDestinationsByListOfOfferIds(offerIdsList)),
            dates: Utils.asyncFromPromise(OfferRepository.getTravelDatesByListOfOfferIds(offerIdsList)),
        }, function (err, results) {
            if (err) {
                console.log(err);
            }
            if (offers.length > 0) {
                offers = _formatOffersForCardItem(req, offers, results.offerDestinations, results.dates);
            }
            const subcategory = Utils.formatCategoriesByLang(results.subcategories, lang)
                .find(sub => sub.id === subcategoryId);
            const subcategoryName = subcategory ? subcategory.name : '';

            res.render('public/subcategory', {
                selectedData,
                title: subcategoryName,
                subcategoryName,
                path: null,
                categories: Utils.getCategoriesWithSubcategories(results.categories, results.subcategories),
                countries: results.countries,
                destinations: results.destinations,
                transportTypes: TransportTypeTranslations[lang],
                months: getNext12MonthsWithYear(lang),
                offers,
                type: ResultType.SEARCH // this is just an example
            });
        });
    }

    let offersByCountry = async (req, res) => {
        const lang = Utils.getCurrentLang(req)

        // TODO: refactor - duplicated code
        let offers = await OfferRepository.getOffersByCountry(req.params.countryId, lang);
        const offerIdsList = offers.map(offer => offer.id);
        const countryId = parseInt(req.params.countryId, 10);

        async.parallel({
            ...defaultQueries,
            destinations: Utils.asyncFromPromise(OfferRepository.getDestinationsByCountry(req.params.countryId, lang)),
            dates: Utils.asyncFromPromise(OfferRepository.getTravelDatesByListOfOfferIds(offerIdsList)),
            countries: Utils.asyncFromPromise(OfferRepository.getAllCountries(lang)),
            offerDestinations: Utils.asyncFromPromise(OfferRepository.getDestinationsByListOfOfferIds(offerIdsList)),
        }, function (err, results) {
            if (err) {
                console.log(err);
            }
            const countriesList = Utils.formatCountriesByLang(results.countries, lang);
            const country = countriesList.find(country => country.id === countryId);
            if (offers.length > 0) {
                offers = _formatOffersForCardItem(req, offers, results.offerDestinations, results.dates);
            }

            res.render('public/offers-by-country', {
                title: "Results",
                path: null,
                categories: Utils.getCategoriesWithSubcategories(results.categories, results.subcategories),
                offers,
                destinationsBlock: results.destinations,
                lang,
                country,
                type: ResultType.SEARCH // this is just an example
            });
        });
    }

    let offersByDestination = async (req, res) => {
        const lang = Utils.getCurrentLang(req)

        // TODO: refactor - duplicated code
        let offers = [];
        try {
            offers = await OfferRepository.getOffersByDestination(req.params.destinationId, lang);
        } catch (error) {
            console.log(error)
        }

        const offerIdsList = offers.map(offer => offer.id);

        async.parallel({
            ...defaultQueries,
            offerDestinations: Utils.asyncFromPromise(OfferRepository.getDestinationsByListOfOfferIds(offerIdsList)),
            dates: Utils.asyncFromPromise(OfferRepository.getTravelDatesByListOfOfferIds(offerIdsList))
        }, function (err, results) {
            if (err) {
                console.log(err);
            }
            if (offers.length > 0) {
                offers = _formatOffersForCardItem(req, offers, results.offerDestinations, results.dates);
            }
            res.render('public/offers-by-country', {
                title: "Results",
                path: null,
                categories: Utils.getCategoriesWithSubcategories(results.categories, results.subcategories),
                offers,
                lang,
                destinationsBlock: [],
                country: null,
                type: ResultType.SEARCH // this is just an example
            });
        });
    }

    let vacations = (req, res) => {

        async.parallel({
            categories: Utils.asyncFromPromise(OfferRepository.getAllCategories())
        }, function (err, results) {

            res.render('public/results', {
                title: 'Vacations',
                path: null,
                categories: results.categories,
                results: [],
                type: ResultType.VACATIONS
            });
        });

    };

    let countryDetails = (req, res) => {
        const lang = Utils.getCurrentLang(req);
        const countryId = parseInt(req.params.id, 10);
        async.parallel({
            ...defaultQueries,
            countries: Utils.asyncFromPromise(OfferRepository.getAllCountries(lang))
        }, (err, results) => {
            const countriesList = Utils.formatCountriesByLang(results.countries, lang);
            const country = countriesList.find(country => country.id === countryId);
            if (!country) {
                res.redirect('/404');
                return;
            }
            res.render('public/country-details', {
                title: country.name,
                path: null,
                categories: Utils.getCategoriesWithSubcategories(results.categories, results.subcategories),
                country
            });
        });

    };

    let bookNow = (req, res) => {
        async.parallel({
            categories: Utils.asyncFromPromise(OfferRepository.getAllCategories())
        }, function (err, results) {

            res.render('public/booking', {
                title: 'Book now',
                path: null,
                categories: results.categories
            });
        });
    };

    let offers = (req, res) => {
        async.parallel({
            categories: Utils.asyncFromPromise(OfferRepository.getAllCategories())
        }, function (err, results) {

            res.render('public/offers', {
                title: 'Offers',
                path: null,
                categories: results.categories
            });
        });
    };

    let subcategoriesByCategory = (req, res) => {
        const lang = Utils.getCurrentLang(req);
        const selectedData = Utils.fetchSelectedData(req);
        const categoryId = parseInt(req.params.id, 10);

        async.parallel({
            ...defaultQueries,
            countries: Utils.asyncFromPromise(OfferRepository.getAllCountries(lang)),
            categories: Utils.asyncFromPromise(OfferRepository.getAllCategories()),
            destinations: Utils.asyncFromPromise(OfferRepository.getAllDestinations(lang)),
            allSubcategories: Utils.asyncFromPromise(OfferRepository.getAllSubcategories()),
            filteredSubcategories: Utils.asyncFromPromise(OfferRepository.getAllSubcategories(categoryId))
        }, function (err, results) {
          const category = Utils.formatCategoriesByLang(results.categories, lang)
              .find(cat => cat.id === categoryId);
          const categoryName = category ? category.name : '';

            res.render('public/subcategories', {
                title: 'Subcategories',
                path: null,
                countries: results.countries,
                categoryName,
                selectedData,
                destinations: results.destinations,
                transportTypes: TransportTypeTranslations[lang],
                months: getNext12MonthsWithYear(lang),
                categories: Utils.getCategoriesWithSubcategories(results.categories, results.allSubcategories),
                subcategories: results.filteredSubcategories
            });
        });
    };

    let aboutUs = (req, res) => {
        const lang = Utils.getCurrentLang(req);
        async.parallel({
            ...defaultQueries,
            categories: Utils.asyncFromPromise(OfferRepository.getAllCategories())
        }, function (err, results) {

            res.render('public/about-us', {
                title: 'About us',
                path: 'about-us',
                categories: Utils.getCategoriesWithSubcategories(results.categories, results.subcategories),
            });
        });
    };

    let contacts = (req, res) => {
        async.parallel({
            ...defaultQueries,
            categories: Utils.asyncFromPromise(OfferRepository.getAllCategories())
        }, function (err, results) {

            res.render('public/contacts', {
                title: 'Contacts',
                path: 'contacts',
                categories: Utils.getCategoriesWithSubcategories(results.categories, results.subcategories),
            });
        });
    };

    let details = (req, res) => {
        // TODO: Трябва да се показвам само активни оферти!
        const lang = Utils.getCurrentLang(req);
        const offerId = req.params.id;
        async.parallel({
            ...defaultQueries,
            hotels: Utils.asyncFromPromise(HotelService.getHotelsByOfferId(offerId)),
            destinations: Utils.asyncFromPromise(OfferRepository.getAllDestinations(lang)),
            offerDestinations: Utils.asyncFromPromise(OfferRepository.getDestinationsByOfferId(offerId)),

            offer: Utils.asyncFromPromise(OfferRepository.getOffer(offerId)),
            photos: Utils.asyncFromPromise(OfferRepository.getOfferPhotos(offerId)),
            translations: Utils.asyncFromPromise(OfferRepository.getOfferTranslations(offerId, lang)),
            dates: Utils.asyncFromPromise(OfferRepository.getOfferTravelDates(offerId)),

        }, (err, results) => {
            try {
                if (err) {
                    console.log(err);
                }
                const offer = results.offer;
                if (!results.offer) {
                    console.error('No offer with id', offerId);
                    res.redirect('/404');
                    return;
                }

                offer.dates = Utils.formatDates(results.dates).join(', ') || '';

                offer.translations = results.translations.shift() || {};
                offer.destinations = results.offerDestinations || [];

                offer['photos'] = FileService.formatPhotos(req, results.photos, appConfig.PUBLIC_PATH_OFFERS);

                const subCategory = results.subcategories.find(subcategory => subcategory.id === offer.subcategoryId);
                const category = results.categories.find(category => category.id === subCategory.categories_id);

                offer.categoryName = category ? category.nameBg : '';
                offer.subCategoryName = subCategory ? subCategory.nameBg : '';
                offer.transportTypeName = TransportType[offer.transportType];

                const hotels = results.hotels.map(hotel => {
                    hotel.photos = FileService.formatPhotosFromName(req, hotel.photos, appConfig.PUBLIC_PATH_HOTEL);
                    hotel.destination = Utils.mapDestinationToHotel(results.destinations, hotel);

                    return hotel;
                });

                res.render('public/offer-detail', {
                    title: 'offer-detail',
                    path: null,
                    categories: Utils.getCategoriesWithSubcategories(results.categories, results.subcategories),
                    hotels,
                    offer
                });

            } catch (error) {
                console.log('Error in Details', error)
            }
        });
    };

    let hotelDetails = (req, res) => {
        const lang = Utils.getCurrentLang(req);
        const hotelId = req.params.id;
        async.parallel({
            ...defaultQueries,
            hotel: Utils.asyncFromPromise(HotelRepository.getById(hotelId)),
            destinations: Utils.asyncFromPromise(OfferRepository.getAllDestinations(lang)),
        }, async (err, results) => {

            if (err) {
                console.log('Error: ', err);
            }
            let hotel = results.hotel.shift();
            let hotelsInSameDestination = [];
            const destinationId = hotel.destination;
            try {

                hotel.destination = Utils.mapDestinationToHotel(results.destinations, hotel);
                hotelsInSameDestination = await HotelRepository.getHotelListByDestinationId(destinationId, hotel.id);

                hotelsInSameDestination.map(hotel => {
                    hotel.photos = FileService.formatPhotosFromName(req, hotel.photos, appConfig.PUBLIC_PATH_HOTEL);
                    hotel.destination = Utils.mapDestinationToHotel(results.destinations, hotel);

                    return hotel;
                });
            } catch (error) {
                console.log(error)
            }
            // TODO: Трябва да се вземе Държавата и да се покаже и тя в описанието; Пример: Санторини, Гърция

            res.render('public/hotel-details', {
                title: 'Documents',
                path: null,
                categories: Utils.getCategoriesWithSubcategories(results.categories, results.subcategories),
                hotel,
                hotelsInSameDestination
            });
        });
    };

    let documents = (req, res) => {
        const lang = Utils.getCurrentLang(req);
        async.parallel({
            ...defaultQueries
        }, function (err, results) {

            res.render('public/documents', {
                title: 'Documents',
                path: 'documents',
                categories: Utils.getCategoriesWithSubcategories(results.categories, results.subcategories),
            });
        });
    };

    let onNotFoundError = (req, res) => {
        async.parallel({
            ...defaultQueries,
        }, (err, results) => {

            res.render('public/404', {
                title: 'Page Not Found',
                path: null,
                categories: Utils.getCategoriesWithSubcategories(results.categories, results.subcategories),
            });
        });
    };

    let _formatOffersForCardItem = (req, offers, offerDestinations, dates) => {
        return offers.map(offer => {
            offer.destinations = offerDestinations.filter(destination => destination.offerId === offer.id) || [];
            const offerTravelDates = dates.filter(dates => dates.offerId === offer.id) || [];
            offer.dates = Utils.formatDates(offerTravelDates);
            const photos = offer.photos.map(photo => photo.path);
            offer.photos = FileService.formatPhotosFromName(req, photos, appConfig.PUBLIC_PATH_OFFERS);
            offer.transport = TransportTypeToTranslationKey[offer.transportType];
            return offer;
        });
    }

    let newsletter = (req, res) => {
        if (!req.body.email || req.body.email.length === 0) {
            res.json({
                success: false
            });
            return;
        }
        AdminRepository.isEmailAdded(req.body.email).then((isAdded) => {
            console.log(isAdded);
            if (isAdded === true) {
                res.json({
                    success: false,
                    msg: 'Email is already added'
                });
            }
            else {
                AdminRepository.addEmailToEmailList(req.body.email).then(() => {
                    res.json({
                        success: true,
                        data: req.body
                    });
                }).catch(function (err) {
                    console.error('addEmailToEmailList', err);
                });
            }
        }).catch(function (err) {
            console.error('isEmailAdded', err);
        });
    };

    return {
        index,
        aboutUs,
        contacts,
        details,
        bookNow,
        offers,
        details,
        documents,
        searchResults,
        vacations,
        countryDetails,
        hotelDetails,
        offersBySubcategory,
        onNotFoundError,
        offersByCountry,
        offersByDestination,
        newsletter,
        subcategoriesByCategory
    };
}