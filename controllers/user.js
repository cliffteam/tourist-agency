'use strict'
/**
 * GET /
 * Home page.
 */
var mysql = require('../lib/mysqlConnection/MysqlConnection'),
    config = require('../config/config');
let UserRepository = require('../api/user/repositories/UserRepository')(mysql, config);

module.exports = function UserController() {
    let alergies = [];

    let displayLoginForm = (req, res) => {
        if (req.isAuthenticated()) {
            res.redirect('/user');
        }
        else{
            res.render('public/user/signin', {
                title: 'Sign in'
            });
        }
    };
    let login = (req, res) => {
        res.json({
            data: req.body
        });
    };
    let displayRegisterForm = (req, res) => {
        if (req.isAuthenticated()) {
            res.redirect('/user');
        }
        else{
            res.render('public/user/signup', {
                title: 'Sign up'
            });
        }
    };
    let registerUser = (req, res) => {
        res.render('public/sub-page', {
            title: 'Home'
        });
    };

    let logout = (req, res) => {
        req.logout();
        res.redirect('/');
    };

    let interests = ['football', 'sports', 'tennis', 'swimming', 'beach', 'footbvall', 'spxorts', 'tennisv', 'swimming', 'beach'];
        // allergies = ['sugar', 'salt', 'cola', 'spirt'],
        // services = ['washing', 'iron', 'aircondition', 'tv']
    let displayUser = (req, res) => {
        if (!req.isAuthenticated()) {
            res.redirect('/sign-in');
        }
        console.log('<><><', req.isAuthenticated(), req.user);

        let outputInterests = {};
        // let outputAllergies = {};
        // let outputServices = {};

        UserRepository.getUserDataById(req.user.userId).then(function(user){
            console.log('>>>', user);
            let userInterests = user.interests.split(',');
            // let userAllergies = user.allergies.split(',');
            // let userServices = user.services.split(',');

            interests.forEach((interest) => {
                outputInterests[interest] = userInterests.indexOf(interest) > -1;
            });
            /*allergies.forEach((interest) => {
                outputAllergies[interest] = userAllergies.indexOf(interest) > -1;
            });
            services.forEach((interest) => {
                outputServices[interest] = userServices.indexOf(interest) > -1;
            });*/
            // let user = {
            //     id: 1,
            //     email: 'm@m.bg',
            //     firstName: 'go6o',
            //     lastName: 'kkkk',
            //     registered_on: '2016-12-01T21:18:57.000Z'
            // };
            res.render('public/user/profile', {
                title: 'Home',
                user: user,
                interests: outputInterests
                // allergies: outputAllergies,
                // services: outputServices
            });
        });
    };
    let allInterests = (req, res) => {
        res.json({
            success: true,
            list: interests
        });
    };
    return {
        displayLoginForm: displayLoginForm,
        login: login,
        displayRegisterForm: displayRegisterForm,
        registerUser: registerUser,
        logout: logout,
        displayUser: displayUser,
        allInterests: allInterests
    };
}