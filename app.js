/**
 * Module dependencies.
 */
const express = require('express');
const compression = require('compression');
const session = require('express-session');
const bodyParser = require('body-parser');
const logger = require('morgan');
const errorHandler = require('errorhandler');
// const lusca = require('lusca');
const dotenv = require('dotenv');
// const MongoStore = require('connect-mongo')(session);
const flash = require('express-flash');
const path = require('path');
const cors = require('cors');
// const mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
const expressValidator = require('express-validator');
const sass = require('node-sass-middleware');
const multer = require('multer');
const upload = multer({ dest: path.join(__dirname, 'uploads') });
const router = require('express').Router();
const i18n = require("i18n-express");

const appConfig = require('./config/main.config');

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
// dotenv.load({ path: '.env.example' });

/**
 * API keys and Passport configuration/.
 */
// const passportConfig = require('./config/passport');

/**
 * Create Express server.
 */

var app = module.exports = express();

/**
 * Connect to MongoDB.
 */
/*mongoose.connect(process.env.MONGODB_URI || process.env.MONGOLAB_URI);
mongoose.connection.on('error', () => {
  console.error('MongoDB Connection Error. Please make sure that MongoDB is running.');
  process.exit(1);
});*/

/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 1915);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(compression());
app.use(sass({
    src: path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public')
}));

app.use(cors({
    origin: true,
    credentials: true
}));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(expressValidator());
app.use(cookieParser());

app.use(session({
    secret: 'T0vaESuperTaenKl1u4',
    saveUninitialized: true,
    resave: true
}));

app.use(i18n({
    translationsPath: path.join(__dirname, 'langs'),
    siteLangs: ["en", "ru", "bg"],
    defaultLang: appConfig.DEFAULT_LANGUAGE,
    textsVarName: 'translation'
}));

app.use((req, res, next) => {
    res.locals.user = req.user;
    next();
});

app.set('jwtTokenSecret', 'samo liteks');

global.catchError = function catchError(res, err, source) {
    if (process.env.NODE_ENV !== 'production') {
        console.log(source, err);
        res.status(400).json({ success: false, msg: err })
    } else {
        res.status(400).json({ success: false })
    }
}

require('./routes')(app);
app.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));

app.use(errorHandler());

global.catchError = function catchError(res, err, source) {
    if (process.env.NODE_ENV !== 'production') {
        console.log(source, err);
        res.json({ success: false, msg: err })
    } else {
        res.json({ success: false })
    }
}

app.listen(app.get('port'), () => {
    console.log('Express server listening on port %d in %s mode', app.get('port'), app.get('env'));
});

module.exports = app;