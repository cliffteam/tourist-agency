const appConfig = require('../config/main.config');
const moment = require('moment');

exports.toCamel = (s) => {
  return s.replace(/([-_][a-z])/ig, ($1) => {
    return $1.toUpperCase()
      .replace('-', '')
      .replace('_', '');
  });
};

exports.asyncFromPromise = function (promise, name) {
  return function (callback) {
    promise
      .then(function (results) {
        return callback(null, results);
      })
      .fail(function (err) {
        return callback(err, null);
      });
  };
};

exports.quotedString = function (str) {
  let result = "'" + str.replace(/'/g, "\\'") + "'";
  return result
};

exports.mapDestinationToHotel = (destinations, hotel) => {
  let destinationName = null;
  const destination = destinations.find((destination) => destination.id === hotel.destination);
  if (destination) {
    destinationName = destination.name;
  }

  return destinationName;
}

exports.getCategoriesWithSubcategories = (categories, subcategories) => {
  subcategories = subcategories || [];
  try {
    categories.forEach(cat => {
      cat['subcategories'] = [];
      for (let i = 0; i < subcategories.length; i++) {
        const subcat = subcategories[i];
        if (subcat.categories_id === cat.id) {
          cat['subcategories'].push(subcat);
        }
      }
    });

  } catch (error) {
    console.log('Error', error);
  }
  return categories;
}

exports.getCurrentLang = (request) => {
  return request.i18n_lang || appConfig.DEFAULT_LANGUAGE;
}

exports.formatDates = (dates) => {
  return dates.map(({ dateFrom, dateTo }) => {
    dateFrom = moment(dateFrom).format(appConfig.DEFAULT_DATE_FORMAT);
    return `${dateFrom}`;
  });
}

exports.formatCountriesByLang = (countries, lang) => {
  return countries.map(country => {
    const output = {
      id: country.id,
      imageUrl: country.imageUrl,
    };
    switch (lang) {
      case 'bg':
        output.name = country.nameBg;
        output.description = country.descBg;
        break;
      case 'en':
        output.name = country.nameEn;
        output.description = country.descEn;
        break;
      case 'ru':
        output.name = country.nameRu;
        output.description = country.descRu;
        break;
      default:
        output.name = country.nameBg;
        output.description = country.descBg;
    }
    return output;
  });
}
exports.formatCategoriesByLang = (categories, lang) => {
  return categories.map(category => {
    switch (lang) {
      case 'bg':
        category.name = category.nameBg;
        break;
      case 'en':
        category.name = category.nameEn;
        break;
      case 'ru':
        category.name = category.nameRu;
        break;
      default:
        category.name = category.nameBg;
    }
    return category;
  });
}
// Move to file
class Hotel {
  constructor(hotel) {
    hotel = hotel || {};
    this.id = hotel.id || 0;
    this.name = hotel.name || null;
    this.createdOn = hotel.created_on || null;
    this.destination = hotel.destination_id || null;
    this.stars = hotel.stars || null;
    this.translations = [];
    this.translationsByLang = {};
    this.photos = [];
  }
}
class HotelTranslation {
  constructor(lang, input) {
    input = input || {};
    this.hotelId = input.hotelId || null;
    this.languageCode = lang;
    this.description = input.description || '';
    this.pricing = input.pricing || '';
  }
}
// TODO: Rename to class HotelsList
exports.formatHotelsListFromDBResult = function (results) {
  let hotels = {};
  results.forEach(resultRow => {
    let hotel = hotels[resultRow.id];
    if (!hotel) {
      hotel = new Hotel(resultRow);
    }
    const translation = new HotelTranslation(resultRow.languageCode, resultRow);

    hotel.translationsByLang[resultRow.languageCode] = translation;
    hotel.translations.push(translation);

    if (hotel.photos.indexOf(resultRow.path) === -1) {
      hotel.photos.push(resultRow.path);
    }
    hotels[resultRow.id] = hotel;
  });

  return Object.keys(hotels).map(hotelId => hotels[hotelId]);
};

exports.extractOfferIdsFromHomeCategories = (homePageOffers) => {
  const offerIdsList = [];
  Object.keys(homePageOffers).forEach(key => {
    homePageOffers[key].offers.forEach(offer => {
      offerIdsList.push(offer.id);
    });
  });
  return offerIdsList;
};

exports.uniqueValues = (array) => {
  const output = []
  array.forEach(value => {
    if (output.indexOf(value) === -1) {
      output.push(value);
    }
  });
  return output;
};

exports.formatCountryTranslations = (country) => {
  return {
    bg: {
      name: country.name_bg,
      description: country.desc_bg
    },
    en: {
      name: country.name_en,
      description: country.desc_en
    },
    ru: {
      name: country.name_ru,
      description: country.desc_ru
    }
  };
}

exports.sortCountryByNameAsc = (lang, countries) => {
  const countryNameField = {
    bg: 'nameBg',
    en: 'nameEn',
    ru: 'nameRu',
  };
  const nameField = countryNameField[lang];
  if (countries && countries.length > 0) {
    countries.sort(function (a, b) { return (a[nameField] > b[nameField]) ? 1 : ((b[nameField] > a[nameField]) ? -1 : 0); });
  }

  return countries;
}

exports.fetchSelectedData = (req) => {
  return {
    country: req.query.country,
    destination: req.query.destination,
    month: req.query.month,
    year: req.query.year,
    transportType: req.query.transport ? transportTypesToId[req.query.transport.toLowerCase()] : undefined
  }
}