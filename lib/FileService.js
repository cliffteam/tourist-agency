const fs = require('fs');
const crypto = require('crypto');

module.exports = function FileService() {

    function addFiles(filesList) {
    }

    function removeFiles(filesList) {
    }

    function createStorage(multer, uploadPath) {
        return multer.diskStorage({
            destination: function (req, file, cb) {
                if (!fs.existsSync(uploadPath)) {
                    fs.mkdirSync(uploadPath);
                }
                cb(null, uploadPath);
            },
            filename: function (req, file, cb) {
                const hashedString = crypto.createHash('md5').update(new Date().getTime().toString()).digest('hex');
                const extensionArray = file.mimetype.split('/');
                const extension = extensionArray[1];
                cb(null, hashedString + '.' + extension);
            }
        });
    }
    //type: hotels; offers (sub-directory of uploads)
    function formatPhotos(req, photos, path) {
        const fullPhotoUrl = `${req.protocol}://${req.get('host')}${path}`;
        return photos.map(photo => {
            photo.path = fullPhotoUrl + photo.path;
            return photo;
        });
    }

    function formatPhotosFromName(req, names, path) {
        const fullPhotoUrl = `${req.protocol}://${req.get('host')}${path}`;
        return names.map(name => {
            name = fullPhotoUrl + name;
            return name;
        });
    }

    return {
        addFiles: addFiles,
        removeFiles: removeFiles,
        createStorage: createStorage,
        formatPhotos: formatPhotos,
        formatPhotosFromName: formatPhotosFromName
    };
};

/* TODO:
Offer.Ctrl.getOfferData => function must return this string; No hardcoded stuff. (Може в Utils)

Routes на Хотел/Оферта
const UPLOADS_PATH = './public/uploads';

var multer = require('multer');
var crypto = require('crypto');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {

        if (!fs.existsSync(UPLOADS_PATH)) {
            fs.mkdirSync(UPLOADS_PATH);
        }
        cb(null, UPLOADS_PATH);
    },
    filename: function (req, file, cb) {
        var hashedString = crypto.createHash('md5').update(new Date().toString()).digest('hex');
        var extensionArray = file.mimetype.split('/');
        var extension = extensionArray[1];
        cb(null, hashedString + '.' + extension);
    }
});
*/
