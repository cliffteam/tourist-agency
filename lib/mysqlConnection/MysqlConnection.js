var connectionPool = require('./ConnectionPool'),
    Q = require('q');

exports.queryFormat = function (mysqlConnection) {
    mysqlConnection.config.queryFormat = function(query, values) {
        if (!values) return query;
        return query.replace(/\:(\w+)/g, function(txt, key) {
            if (values.hasOwnProperty(key)) {
                return this.escape(values[key]);
            }
            return txt;
        }.bind(this));
    };
    return mysqlConnection;
};

exports.makeQuery = function(query, args, filter, debug) {
    var deferred = Q.defer(),
        me = this;

    if (!filter || typeof filter !== 'function') {
        filter = function (_) {return _;};
    }

    connectionPool.getConnection(function (err, connection) {
        if (err) {
            return  deferred.reject(err);
        }
        connection = me.queryFormat(connection);

        var q = connection.query(query, args, function (err, results) {
            if (debug === true) {
                console.log('makeQuery', q.sql);
            }
            connection.release();
            if (err) {
                return deferred.reject(err);
            }
            return deferred.resolve(filter(results));
        });
    });

    return deferred.promise;
};
exports.escape = function(string){
    return connectionPool.escape(string);
};