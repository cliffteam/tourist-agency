'use strict'
var router = require('express').Router(),
    path = require('path'),
    homeController = require('../controllers/home')(),
    userController = require('../controllers/user')(),
    adminController = require('../controllers/admin/home'),
    config = require('../config/config'),
    pathServer = path.join(__dirname, '../');
    // pathLib = pathServer + 'lib/',
    // config = require('../config');
module.exports = function(app) {

    let user = require(pathServer + 'api/user')(app);
    let offers = require(pathServer + 'api/offer')();
    let hotel = require(pathServer + 'api/hotel')();

    app.use('/', router);
    app.get('/', homeController.index);// A1, A2
    app.get('/404', homeController.onNotFoundError);// A1, A2
    app.get('/book-now', homeController.bookNow); //TODO[20/01/2019]Martin: Do we still use it?
    // app.get('/offers', homeController.offers);
    app.get('/documents', homeController.documents);
    app.get('/about-us', homeController.aboutUs);
    app.get('/contacts', homeController.contacts);
    app.get('/details/:id', homeController.details); //B3, B4, C3, D2,, E2, F2, G2
    /*TODO[20/01/2019]Martin: Do we have users???*/
    app.get('/user', userController.displayUser);

    app.get('/sign-in', userController.displayLoginForm);
    app.post('/login', userController.login);
    app.get('/sign-up', userController.displayRegisterForm);
    app.post('/register', userController.registerUser);
    app.get('/logout', userController.logout);
    /*TODO[20/01/2019]Martin: Do we have users???*/

    //The commented out endpoints will be created when we connect the backend (use generic pages as *details* for now)
    app.get('/results', homeController.searchResults);// A3, A4, A5,
    app.get('/vacations', homeController.vacations);//B1, B2 //add location query param for fetching by location
    // app.get('/vacations/:id', homeController.vacationDetails);
    app.get('/hotels/:id', homeController.hotelDetails);
    app.get('/countries/:id', homeController.countryDetails);//B5, C4
    app.get('/subcategories/:id', homeController.offersBySubcategory);
    app.get('/categories/:id', homeController.subcategoriesByCategory);
    app.get('/countries/:countryId/offers', homeController.offersByCountry);
    app.get('/destinations/:destinationId/offers', homeController.offersByDestination);
    // app.get('/excursions', homeController.excursions);//shows two types if no query param | if query param -> shows all of the type
    // app.get('/excursions/:id', homeController.excursionDetails);
    // app.get('/destinations/:id', homeController.destinationDetails);
    // app.get('/promo', homeController.destinationDetails);


    // app.post(config.apiPrefix + '/contacts', adminController.contacts);
    app.post(config.apiPrefix + '/newsletter', homeController.newsletter);

    app.get('/admin', adminController.index);

    app.use(config.apiPrefix + '/user', user);
    app.use(config.apiPrefix + '/offers', offers);
    app.use(config.apiPrefix + '/hotel', hotel);
    return router;
};
