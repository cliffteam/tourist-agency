ALTER TABLE `tour_agency`.`offer_translation`
ADD COLUMN `promo_title` VARCHAR(255) NULL DEFAULT NULL AFTER `name`;
/** Date: 01.06.2019 */

ALTER TABLE `tour_agency`.`offers`
ADD COLUMN `is_active` TINYINT(1) NULL DEFAULT 1 AFTER `is_promo`;
/** Date: 01.08.2019 */

ALTER TABLE `tour_agency`.`offer_has_destination`
ADD COLUMN `id` INT NOT NULL AUTO_INCREMENT AFTER `destination_id`,
ADD PRIMARY KEY (`id`);
;
/** Date: 13.08.2019 */

ALTER TABLE `tour_agency`.`offers`
DROP COLUMN `level`,
ADD COLUMN `level_slider` INT NOT NULL DEFAULT 0 AFTER `created_at`,
ADD COLUMN `level_subcategory` INT NOT NULL DEFAULT 0 AFTER `level_slider`;

/** Date: 15.08.2019 made by Martin */

ALTER TABLE `tour_agency`.`offer_has_destination` 
ADD COLUMN `id` INT UNSIGNED NOT NULL AUTO_INCREMENT AFTER `destination_id`,
ADD PRIMARY KEY (`id`);

ALTER TABLE `tour_agency`.`offer_has_destination` 
CHANGE COLUMN `id` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT FIRST;

/** Date: 19.08.2019 made by Martin */
DROP TRIGGER IF EXISTS `tour_agency`.`destination_BEFORE_DELETE`;

DELIMITER $$
USE `tour_agency`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `tour_agency`.`destination_BEFORE_DELETE` BEFORE DELETE ON `destination` FOR EACH ROW
BEGIN
    DELETE FROM destination_translation WHERE destination_id=OLD.id;
    DELETE FROM destination_photos WHERE destination_id=OLD.id;
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `tour_agency`.`offers_BEFORE_DELETE`;

DELIMITER $$
USE `tour_agency`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `tour_agency`.`offers_BEFORE_DELETE` BEFORE DELETE ON `offers` FOR EACH ROW
BEGIN
	DELETE FROM offer_travel_dates WHERE offer_id=OLD.id;
    DELETE FROM offer_translation WHERE offer_id=OLD.id;
    DELETE FROM offers_has_hotel WHERE offers_id=OLD.id;
    DELETE FROM offer_photos WHERE offer_id=OLD.id;
    DELETE FROM offer_has_destination WHERE offers_id=OLD.id;
END$$
DELIMITER ;
