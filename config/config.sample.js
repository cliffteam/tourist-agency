const config = {
    port: 1915,
    apiPrefix: '/apiv1',
    database: {
        host: 'localhost',
        user: '',
        password: '',
        database: '',
        multipleStatements: true,
        acquireTimeout: 30000
    }
};
module.exports = config;
