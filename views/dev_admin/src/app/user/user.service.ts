import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable()
export class UserService {

    userApiUrl: string = env.apiUrl + 'user/';

    constructor(private http: HttpClient) { }

    login(username: string, password: string) {
        return this.http.post<any>(this.userApiUrl + 'login', {
            email: username, password: password
        }).pipe(map(user => {
            if (user && user.token) {
                localStorage.setItem('currentUser', JSON.stringify(user));
            }

            return user;
        }));
    }
    isUserLogged(): boolean {
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && new Date().getTime() < currentUser.expires) {
            return true;
        } else {
            return false;
        }
    }
    subscribersList() {
        return this.http.get(this.userApiUrl + 'subscribers');
    }

    logout() {
        localStorage.removeItem('currentUser');
    }
}
