import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user.service';
import { TableData } from '../config/app';
import { ISubcategory } from '../_declarations/main';
import { SubcategoriesService } from '../subcategories/subcategories.service';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
    public usersTable: TableData;
    public subcategoriesList: ISubcategory[] = [];
    public selectedSubCategories: any;

    constructor(
        private subcategoriesService: SubcategoriesService,
        private userService: UserService
    ) { }

    ngOnInit() {
        this.selectedSubCategories = {
            subCategory1: null,
            subCategory2: null,
            subCategory3: null,
        };
        this.subcategoriesService.loadSubcategoriesList().subscribe(subcategoriesList => {
            this.subcategoriesList = subcategoriesList;
        }, error => console.error);

        this.usersTable = {
            headerRow: ['#', 'Име', 'Абониран на дата'],
            dataRows: []
        };
        this.userService.subscribersList().subscribe(list => {
            this.usersTable.dataRows = <any>list;
        });

        this.subcategoriesService.getHomePageSubCategories().subscribe(result => {
            if (result && result.subcategories) {

                this.selectedSubCategories.subCategory1 = result.subcategories.subCategory1.subcategoryId
                this.selectedSubCategories.subCategory2 = result.subcategories.subCategory2.subcategoryId
                this.selectedSubCategories.subCategory3 = result.subcategories.subCategory3.subcategoryId
                console.log(result);
            }
        }, error => console.error);
    }

    onSaveSubCategories(){
        this.subcategoriesService.saveHomePageSubCategories(this.selectedSubCategories).subscribe(result => {
            if (result.success) {
                alert("Категориите бяха записани успешно");
            }
        }, error => console.error);

    }
}
