import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { UserComponent } from './user/user.component'
import { NotificationsComponent } from './notifications/notifications.component';
import { AuthGuard } from './_guards/auth.guard';
import { LoginComponent } from './login/login.component';
import { SubcategoriesComponent } from './subcategories/subcategories.component';
import { OffersComponent } from './offers/offers.component';
import { EditSubcategoryComponent } from './subcategories/edit-subcategory/edit-subcategory.component';
import { EditOfferComponent } from './offers/edit-offer/edit-offer.component';
import { HotelComponent } from './hotel/hotel.component';
import { EditHotelComponent } from './hotel/edit-hotel/edit-hotel.component';
import { CountryComponent } from './country/country.component';
import { EditCountryComponent } from './country/edit-country/edit-country.component';
import { DestinationComponent } from './destination/destination.component';
import { EditDestinationComponent } from './destination/edit-destination/edit-destination.component';

const routes: Routes = [
    { path: 'dashboard', component: UserComponent, canActivate: [AuthGuard] },
    { path: 'subcategories', component: SubcategoriesComponent, canActivate: [AuthGuard] },
    { path: 'subcategories/new', component: EditSubcategoryComponent, canActivate: [AuthGuard] },
    { path: 'subcategories/:id', component: EditSubcategoryComponent, canActivate: [AuthGuard] },
    { path: 'offers', component: OffersComponent, canActivate: [AuthGuard] },
    { path: 'hotels', component: HotelComponent, canActivate: [AuthGuard] },
    { path: 'hotels/new', component: EditHotelComponent, canActivate: [AuthGuard] },
    { path: 'hotels/:id', component: EditHotelComponent, canActivate: [AuthGuard] },
    { path: 'countries', component: CountryComponent, canActivate: [AuthGuard] },
    { path: 'countries/new', component: EditCountryComponent, canActivate: [AuthGuard] },
    { path: 'countries/:id', component: EditCountryComponent, canActivate: [AuthGuard] },
    { path: 'destinations', component: DestinationComponent, canActivate: [AuthGuard] },
    { path: 'destinations/new', component: EditDestinationComponent, canActivate: [AuthGuard] },
    { path: 'destinations/:id', component: EditDestinationComponent, canActivate: [AuthGuard] },
    { path: 'offers/new', component: EditOfferComponent, canActivate: [AuthGuard] },
    { path: 'offers/:id', component: EditOfferComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    // { path: 'typography',     component: TypographyComponent },
    // { path: 'icons',          component: IconsComponent },
    // { path: 'maps',           component: MapsComponent },
    { path: 'notifications', component: NotificationsComponent },
    // { path: 'upgrade',        component: UpgradeComponent },
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes, {useHash: true})
    ],
    exports: [
    ],
})
export class AppRoutingModule { }
