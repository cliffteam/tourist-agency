import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';

@Injectable()
export class CountriesService {

    countriesApiUrl: string = env.apiUrl + 'offers/countries/';

    constructor(private http: HttpClient) { }

    loadList(): Observable<any> {
        return this.http.get(this.countriesApiUrl);
    }

    findOne(countryId: number): Observable<any> {
        return this.http.get(this.countriesApiUrl + countryId);
    }

    create(data: any): Observable<any> {
        return this.http.post(this.countriesApiUrl, data);
    }

    update(countryId: number, data: any): Observable<any> {
        return this.http.put(this.countriesApiUrl + countryId, data);
    }

    delete(countryId: number): Observable<any> {
        return this.http.delete(this.countriesApiUrl + countryId);
    }

    deleteImage(countryId: number): Observable<any> {
        return this.http.delete(`${this.countriesApiUrl}photos/${countryId}`);
    }

}
