import { Component, OnInit, OnDestroy } from '@angular/core';
import { CountriesService } from './country.service';
import { ICountry } from '../_declarations/main';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-country',
    templateUrl: './country.component.html',
    styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit, OnDestroy {
    private subscription: Subscription = new Subscription();
    public countriesList: ICountry[] = [];

    constructor(
        private countriesService: CountriesService,
    ) { }

    ngOnInit() {
        this.loadList();
    }

    deleteCountry(country: ICountry) {
        const confirmDelete = window.confirm(`Are you sure you want to delete ${country.nameBg}?`);
        if (confirmDelete) {
            const sub = this.countriesService.delete(country.id).subscribe(response => {
                this.loadList();
            }, error => {
                console.error('Delete Dynamic Template:', error.error.msg);
            });
            //TODO: Error handling with Notifications
            this.subscription.add(sub);
        }
    }
    private loadList() {
        this.countriesService.loadList().subscribe(result => {
            this.countriesList = result.result;
        }, console.error);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
