import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { ICountry } from '../../_declarations/main';
import { Country } from '../../_models/country.model';
import { CountriesService } from '../country.service';
import { Location } from '@angular/common';
import { Subscription, Observable } from 'rxjs';

@Component({
    selector: 'app-edit-country',
    templateUrl: './edit-country.component.html',
    styleUrls: ['./edit-country.component.css']
})
export class EditCountryComponent implements OnInit, OnDestroy {

    @ViewChild('fileUploadInput', { static: false }) fileUploadInput: ElementRef;

    countryForm: FormGroup;
    sub: Subscription;
    data: ICountry = new Country();
    imageUpdated: boolean = false;

    errorMessage: string = null;
    constructor(
        private router: Router,
        private location: Location,
        private route: ActivatedRoute,
        private countriesService: CountriesService,
        private sanitizer: DomSanitizer
    ) { }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            const id = +params.id;
            if (id && id > 0) {
                this.countriesService.findOne(id).subscribe(res => {
                    let country = res.country;
                    if (country) {
                        this.data = new Country(country);
                    } else {
                        console.log("Country not found");
                        // this.location.back();
                    }
                });
            }
        });
    }

    onSubmit(): void {
        this.errorMessage = null;
        let subscribtion: Observable<any>;
        let country = { ...this.data };

        let formData = new FormData();

        if (this.imageUpdated && country.photo && country.photo.file) {
            formData.append('photo', country.photo.file);
            country.photo.file = null; //Clean file before JSON.stringify;
        }

        formData.append('country', JSON.stringify(country));
        console.log("EDIT C")
        console.log(formData, "FORM");

        if (this.data && this.data.id > 0) {
            subscribtion = this.countriesService.update(this.data.id, formData);
        } else {
            subscribtion = this.countriesService.create(formData);
        }
        subscribtion.subscribe(result => {
            if (result.success === true) {
                this.router.navigate(['/countries']);
            } else {
                this.errorMessage = result.msg || result.message;
            }
        });
    }

    onImagesChange(event) {
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            let file = event.target.files[0];
            reader.onload = (event: any) => {
                let newImage = {
                    path: window.URL.createObjectURL(file),
                    file: file
                };
                this.data.photo = newImage;
            }

            reader.readAsDataURL(file);
            this.imageUpdated = true;
            this.fileUploadInput.nativeElement.value = null;
        }
    }

    deleteImage() {
        if (window.confirm('Сигурни ли сте, че искате да изтриете снимката?')) {
            this.countriesService.deleteImage(this.data.id).subscribe(result => {
            }, error => {
                alert('Грешка при триене на снимка');
            });
            // this.imageUpdated = false;
            this.data.photo = null;
        }
    }

    sanitize(url: string) {
        console.log(this.sanitizer.bypassSecurityTrustStyle(`url(${url})`))
        return this.sanitizer.bypassSecurityTrustStyle(`url(${url})`);
    }

    /*mapCountryTranslations(country: ICountry) {
        country.translations.forEach((transition: CountryTranslation) => {
            country[transition.languageCode] = transition;
        });
        return country;
    }*/

    cancel() {
        this.location.back();
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
}
