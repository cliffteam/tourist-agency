import { OfferTranslation } from "../_models/offer-translation";
import { HotelTranslation } from "../_models/hotel.model";
import { DestinationTranslation } from "../destination/destination.model";

/**Shared */

interface IPhoto {
    path: string;
    id?: number;
    file?: Blob;
}
interface IHotelPhoto extends IPhoto {
    hotelId?: number;
}
interface IOfferPhoto extends IPhoto {
    offerId?: number;
    isHead: boolean;
}
interface IDestinationPhoto extends IPhoto {
    destinationId?: number;
}

export interface ICategory {
    id: number;
    nameEn: string;
    nameRu: string;
    nameBg: string;
    imageUrl?: any;
    createdOn: Date;
}

export interface ISubcategory {
    id: number;
    nameEn: string;
    nameRu: string;
    nameBg: string;
    categoryId: number;
    createdOn: Date;
    photo?: any;
}

/**Offer */
export interface IOffer {
    id: number;
    subcategoryId: number;
    refId: string;
    transportType: number;
    type: any; //Почивка Екскурзия
    dates: IDates[];
    photos: IOfferPhoto[];
    price: number;
    promoPrice: number;
    isPromo: boolean;
    isActive: boolean;
    hotels: IHotel[];
    translations: OfferTranslation[];
    headPhotoIndex: number;
    sliderLevel: number;
    subcategoryLevel: number;
    destinations: any[];
}

export interface IDates {
    dateFrom: IDay | Date;
    dateTo: IDay | Date;
    stay?: number;
}

interface IDay {
    year: number;
    month: number;
    day: number;
}


/**Hotel */
export interface IHotel {
    id: number;
    name: string;
    photos: IHotelPhoto[];
    destination: number;
    stars: number;
    translations: HotelTranslation[]
}

/**Country */
export interface ICountry {
    id: number;
    nameEn: string;
    nameRu: string;
    nameBg: string;
    photo?: any;
    descEn: string;
    descRu: string;
    descBg: string;
}

/**Destination */
export interface IDestination {
    id: number;
    countryId: number;
    bg?: DestinationTranslation;
    en?: DestinationTranslation;
    ru?: DestinationTranslation;
    translations: DestinationTranslation[]
    photos: any[];
}
