import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';

@Injectable()
export class HotelsService {

    hotelsApiUrl: string = env.apiUrl + 'hotel/';

    constructor(private http: HttpClient) { }

    loadList(): Observable<any> {
        return this.http.get(this.hotelsApiUrl);
    }

    findOne(hotelId: number): Observable<any> {
        return this.http.get(this.hotelsApiUrl + hotelId);
    }

    create(data: any): Observable<any> {
        return this.http.post(this.hotelsApiUrl, data);
    }

    update(hotelId: number, data: any): Observable<any> {
        return this.http.put(this.hotelsApiUrl + hotelId, data);
    }

    delete(hotelId: number): Observable<any> {
        return this.http.delete(this.hotelsApiUrl + hotelId);
    }

    deleteImage(imageId: number): Observable<any> {
        return this.http.delete(`${this.hotelsApiUrl}photos/${imageId}`);
    }

}
