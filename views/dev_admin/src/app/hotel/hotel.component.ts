import { Component, OnInit, OnDestroy } from '@angular/core';
import { HotelsService } from './hotel.service';
import { IHotel } from '../_declarations/main';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-hotel',
    templateUrl: './hotel.component.html',
    styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit, OnDestroy {
    private subscription: Subscription = new Subscription();
    public hotelsList: IHotel[] = [];

    constructor(
        private hotelsService: HotelsService,
    ) { }

    ngOnInit() {
        this.loadList();
    }

    deleteHotel(hotel: IHotel) {
        const confirmDelete = window.confirm(`Are you sure you want to delete ${hotel.name}?`);
        if (confirmDelete) {
            const sub = this.hotelsService.delete(hotel.id).subscribe(response => {
                this.loadList();
            }, error => {
                console.error('Delete Dynamic Template:', error.error.msg);
            });
            //TODO: Error handling with Notifications
            this.subscription.add(sub);
        }
    }
    private loadList() {
        this.hotelsService.loadList().subscribe(result => {
            console.log('>>loadList>', result);
            this.hotelsList = result.hotels;
        }, console.error);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
