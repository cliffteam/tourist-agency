import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { IHotel, IDestination } from '../../_declarations/main';
import { HotelsService } from '../hotel.service';
import { SITE_TRANSLATIONS_LIST } from '../../config/app.config';
import { Location } from '@angular/common';
import { DestinationsService } from '../../destination/destination.service';
import { Subscription, Observable } from 'rxjs';
import { Hotel } from '../../_models/hotel.model';

@Component({
    selector: 'app-edit-hotel',
    templateUrl: './edit-hotel.component.html',
    styleUrls: ['./edit-hotel.component.css']
})
export class EditHotelComponent implements OnInit, OnDestroy {
    @ViewChild('fileUploadInput', {static: false}) fileUploadInput: ElementRef;

    hotelForm: FormGroup;
    sub: Subscription;
    data: IHotel = new Hotel();
    categoriesList: any[]; //TODO: Interface;
    transportsList: string[] = ['Самолет', 'Автобус'];

    errorMessage: string = null;
    languages: string[];
    languagesMapping: any = SITE_TRANSLATIONS_LIST;
    public destinationsList: IDestination[] = [];

    constructor(
        private router: Router,
        private location: Location,
        private route: ActivatedRoute,
        private hotelsService: HotelsService,
        private destinationsService: DestinationsService,
        private sanitizer: DomSanitizer
    ) { }

    ngOnInit() {
        this.languages = Object.keys(this.languagesMapping);

        this.destinationsService.loadList().subscribe(result => {
            this.destinationsList = result.destinations;
        }, error => {
            console.error(error);
        });

        this.sub = this.route.params.subscribe(params => {
            const id = +params.id;
            if (id && id > 0) {
                this.hotelsService.findOne(id).subscribe(res => {
                    let hotel = res.hotel;
                    if (hotel) {
                        this.data = new Hotel(hotel);
                    } else {
                        // this.location.back();
                    }
                });
            }
        });
    }

    onSubmit(): void {
        let subscribtion: Observable<any>;
        let hotel = { ...this.data };

        const formData = new FormData();
        hotel.photos.forEach(photo => {
            if (photo.file) {
                formData.append('photos', photo.file);
                photo.file = null; //Clean file before JSON.stringify;
            }
        });

        formData.append('hotel', JSON.stringify(hotel));

        if (hotel && hotel.id > 0) {
            subscribtion = this.hotelsService.update(hotel.id, formData);
        } else {
            subscribtion = this.hotelsService.create(formData);
        }
        subscribtion.subscribe(result => {
            if (result.success === true) {
                this.router.navigate(['/hotels']);
            } else {
                this.errorMessage = result.msg || result.message;
            }
        });
    }

    onImagesChange(event) {
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            let file = event.target.files[0];
            reader.onload = (event: any) => {
                let newImage = {
                    path: window.URL.createObjectURL(file),
                    file: file
                };
                this.data.photos.push(newImage);
            }

            this.fileUploadInput.nativeElement.value = null;
            reader.readAsDataURL(file);
        }
    }

    deleteImage(image: any, index: number) {
        if (image.id) {
            if (window.confirm('Сигурни ли сте, че искате да изтриете снимката?')) {
                this.hotelsService.deleteImage(image.id).subscribe(result => {
                    this.data.photos.splice(index, 1);
                }, error => {
                    console.error('>>>erorr', error);
                    alert('Error in image delete');
                })
            }
        } else {
            this.data.photos.splice(index, 1);
        }
    }

    sanitize(url: string) {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${url})`);
    }

    cancel() {
        this.location.back();
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
}
