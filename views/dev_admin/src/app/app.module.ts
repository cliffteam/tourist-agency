import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import 'froala-editor/js/plugins.pkgd.min.js';

import { AppRoutingModule } from './app.routing';
import { NavbarModule } from './shared/navbar/navbar.module';
import { FooterModule } from './shared/footer/footer.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { LbdModule } from './lbd/lbd.module';
// import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { AppComponent } from './app.component';

import { UserComponent } from './user/user.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UpgradeComponent } from './upgrade/upgrade.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { LoginComponent } from './login/login.component';
import { EditSubcategoryComponent } from './subcategories/edit-subcategory/edit-subcategory.component';
import { EditOfferComponent } from './offers/edit-offer/edit-offer.component';

import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HotelComponent } from './hotel/hotel.component';
import { EditHotelComponent } from './hotel/edit-hotel/edit-hotel.component';
import { CountryComponent } from './country/country.component';
import { EditCountryComponent } from './country/edit-country/edit-country.component';
import { DestinationComponent } from './destination/destination.component';
import { EditDestinationComponent } from './destination/edit-destination/edit-destination.component';
import { HotelsService } from './hotel/hotel.service';
import { CountriesService } from './country/country.service';
import { DestinationsService } from './destination/destination.service';
import { SubcategoriesComponent } from './subcategories/subcategories.component';
import { OffersComponent } from './offers/offers.component';
import { AuthGuard } from './_guards/auth.guard';
import { SubcategoriesService } from './subcategories/subcategories.service';
import { OffersService } from './offers/offers.service';
import { UserService } from './user/user.service';
import { JwtInterceptor } from './_helpers/jwt.interceptor';

@NgModule({
    declarations: [
        AppComponent,
        SubcategoriesComponent,
        UserComponent,
        OffersComponent,
        TypographyComponent,
        IconsComponent,
        MapsComponent,
        NotificationsComponent,
        UpgradeComponent,
        LoginComponent,
        EditSubcategoryComponent,
        EditOfferComponent,
        HotelComponent,
        EditHotelComponent,
        CountryComponent,
        EditCountryComponent,
        DestinationComponent,
        EditDestinationComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        NavbarModule,
        FooterModule,
        SidebarModule,
        RouterModule,
        AppRoutingModule,
        LbdModule,
        HttpClientModule,
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        NgbModule,
        // NgMultiSelectDropDownModule.forRoot()
    ],
    providers: [
        AuthGuard,
        HttpClientModule,
        SubcategoriesService,
        OffersService,
        UserService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
        HotelsService,
        CountriesService,
        DestinationsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
