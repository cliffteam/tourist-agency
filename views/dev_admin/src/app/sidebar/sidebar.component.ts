import { Component, OnInit } from '@angular/core';
declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: 'dashboard', title: 'Dashboard', icon: 'pe-7s-graph', class: '' },
    { path: 'place/:id', title: 'Place #1', icon: 'pe-7s-paperclip', class: '' },
    /*{ path: 'typography', title: 'Typography',  icon:'pe-7s-news-paper', class: '' },
    { path: 'icons', title: 'Icons',  icon:'pe-7s-science', class: '' },
    { path: 'maps', title: 'Maps',  icon:'pe-7s-map-marker', class: '' },
    { path: 'notifications', title: 'Notifications',  icon:'pe-7s-bell', class: '' },*/
    { path: 'settings', title: 'Settings', icon: 'pe-7s-user', class: 'active-pro' },
    // { path: 'upgrade', title: 'Upgrade to PRO',  icon:'pe-7s-rocket', class: '' },
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
    menuItems: any[];

    constructor() { }

    ngOnInit() {
        this.menuItems = [
            { path: 'dashboard', name: 'Работен плот', icon: 'pe-7s-graph', class: '' },
            { path: 'subcategories', name: 'Подкатегории', icon: 'pe-7s-graph', class: '' },
            { path: 'hotels', name: 'Хотели', icon: 'pe-7s-hotel', class: '' },
            { path: 'countries', name: 'Държави', icon: 'pe-7s-hotel', class: '' },
            { path: 'destinations', name: 'Дестинации', icon: 'pe-7s-hotel', class: '' },
            { path: 'offers', name: 'Оферти', icon: 'pe-7s-user', class: '' }
        ];
    }
    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
}
