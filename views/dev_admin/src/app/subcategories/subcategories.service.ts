import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';


@Injectable()
export class SubcategoriesService {

    subcategoriesApiUrl: string = env.apiUrl + 'offers/subcategories/';
    categoriesApiUrl: string = env.apiUrl + 'offers/categories/';

    constructor(private http: HttpClient) { }

    loadCategoriesList(): Observable<any> {
        return this.http.get(this.categoriesApiUrl);
    }

    loadSubcategoriesList(): Observable<any> {
        return this.http.get(this.subcategoriesApiUrl);
    }

    findOne(id: number): Observable<any> {
        return this.http.get(this.subcategoriesApiUrl + id);
    }

    create(category: any): Observable<any> {
        return this.http.post(this.subcategoriesApiUrl + 'new', category);
    }

    update(id: number, category: any): Observable<any> {
        return this.http.put(this.subcategoriesApiUrl + id, category);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(this.subcategoriesApiUrl + id);
    }

    deleteImage(subcategoryId: number): Observable<any> {
        return this.http.delete(`${this.subcategoriesApiUrl}photos/${subcategoryId}`);
    }

    saveHomePageSubCategories(selectedSubCategories: any): Observable<any> {
        return this.http.put(this.subcategoriesApiUrl + 'home-page', selectedSubCategories);
    }

    getHomePageSubCategories(): Observable<any> {
        return this.http.get(this.subcategoriesApiUrl + 'home-page');
    }
}
