import { Component, OnInit } from '@angular/core';
import { SubcategoriesService } from '../subcategories/subcategories.service';
import { TableData } from '../config/app';

export const ERROR_NOT_AUTHORIZED = 'NOT_AUTHORIZED';

@Component({
    selector: 'app-subcategories',
    templateUrl: './subcategories.component.html',
    styleUrls: ['./subcategories.component.css']
})
export class SubcategoriesComponent implements OnInit {
    public subcategoriesTable: TableData;

    constructor(
        private subcategoriesService: SubcategoriesService
    ) { }

    ngOnInit() {

        this.subcategoriesTable = {
            headerRow: ['#', 'Име'],
            dataRows: []
        };
        this.loadSubcategoriesList();
    }

    deleteCategory(id) {
        this.subcategoriesService.delete(id).subscribe(result => {
            this.loadSubcategoriesList();
        }, errorRes => {
            alert(errorRes.error.msg);
        });
    }

    private loadSubcategoriesList() {
        this.subcategoriesService.loadSubcategoriesList().subscribe(subcategoriesList => {
            this.subcategoriesTable.dataRows = subcategoriesList;
        }, errorRes => {
            alert(errorRes.error.msg);
        });
    }
}
