import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SubcategoriesService } from '../../subcategories/subcategories.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { Location } from '@angular/common';
import { ICategory, ISubcategory } from '../../_declarations/main';
import { Subcategory } from '../../_models/subcategory.model';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-edit-subcategory',
    templateUrl: './edit-subcategory.component.html',
    styleUrls: ['./edit-subcategory.component.css']
})
export class EditSubcategoryComponent implements OnInit, OnDestroy {

    @ViewChild('fileUploadInput', { static: false }) fileUploadInput: ElementRef;

    subcategoryForm: FormGroup;
    sub: Subscription;
    data: ISubcategory = new Subcategory();
    categoriesList: ICategory[] = []
    imageUpdated: boolean = false;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private fb: FormBuilder,
        private subcategoriesService: SubcategoriesService,
        private location: Location,
        private sanitizer: DomSanitizer
    ) { }

    ngOnInit() {
        this.subcategoriesService.loadCategoriesList().subscribe(categoriesList => {
            this.categoriesList = categoriesList;
        });
        this.createForm();
        this.sub = this.route
            .params
            .subscribe(params => {
                const id = +params.id;
                if (id) {
                    this.subcategoriesService.findOne(id).subscribe(subcategory => {
                        if (subcategory) {
                            this.data = new Subcategory(subcategory);
                            this.subcategoryForm.patchValue(subcategory);
                        } else {
                            this.location.back();
                        }
                    });
                }
            });
    }

    private createForm() {
        this.subcategoryForm = this.fb.group({
            nameBg: ['', Validators.required],
            nameEn: ['', Validators.required],
            nameRu: ['', Validators.required],
            categoryId: ['', Validators.required]
        });
    }
    onSubmit(): void {
        let subscribtion: Observable<any>;

        let formData = new FormData();

        let subcategory = { ...this.data };
        if (this.imageUpdated && subcategory.photo && subcategory.photo.file) {
            formData.append('photo', subcategory.photo.file);
            subcategory.photo.file = null; //Clean file before JSON.stringify;
        }

        formData.append('subcategory', JSON.stringify(subcategory));

        if (this.data && this.data.id > 0) {
            subscribtion = this.subcategoriesService.update(this.data.id, formData);
        } else {
            subscribtion = this.subcategoriesService.create(formData);
        }
        subscribtion.subscribe(result => {
            if (result.success === true) {
                this.router.navigate(['/subcategories']);
            } else {
                alert(result.msg || result.message);
            }
        });
    }

    onImagesChange(event) {
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            let file = event.target.files[0];
            reader.onload = (event: any) => {
                let newImage = {
                    path: window.URL.createObjectURL(file),
                    file: file
                };
                this.data.photo = newImage;
            }

            reader.readAsDataURL(file);
            this.imageUpdated = true;
            this.fileUploadInput.nativeElement.value = null;
        }

    }

    deleteImage() {
        if (window.confirm('Сигурни ли сте, че искате да изтриете снимката?')) {
            this.subcategoriesService.deleteImage(this.data.id).subscribe(result => {
            }, error => {
                alert('Грешка при триене на снимка');
            });
            // this.imageUpdated = false;
            this.data.photo = null;
        }
    }

    cancel() {
        this.location.back();
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    sanitize(url: string) {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${url})`);
    }
}
