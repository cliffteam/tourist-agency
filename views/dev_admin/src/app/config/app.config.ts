export const SITE_TRANSLATIONS_LIST = {
    bg: 'български',
    en: 'английски',
    ru: 'руски'
};

export enum SiteTranslations {
    BG = 'bg',
    EN = 'en',
    RU = 'ru'
};
