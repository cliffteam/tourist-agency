import { SITE_TRANSLATIONS_LIST } from "../config/app.config";
import { OfferTranslation } from "./offer-translation";
import { IOffer, IDates, IHotel } from "../_declarations/main";


export function resolveDateToDatePicker(date) {
    const d = new Date(date);
    /*
        year: d.getUTCFullYear(),
        month: d.getUTCMonth() + 1,
        day: d.getUTCDate()
    */
    return {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    };
};

export const resolveDateFromDatepicker = (date) => {
    return date ? new Date(date.year, date.month - 1, date.day) : null;
};

export class Offer implements IOffer {
    id: number;
    subcategoryId: number;
    refId: string;
    type: any;
    transportType: number;
    price: number;
    promoPrice: number;
    isPromo: boolean;
    isActive: boolean;
    destinations: any[];
    dates: IDates[];
    photos: any[];
    hotels: IHotel[];
    translations: OfferTranslation[];
    headPhotoIndex: number;
    sliderLevel: number;
    subcategoryLevel: number;


    constructor(offer?) {
        offer = offer || {};
        this.id = offer.id || 0;
        this.subcategoryId = offer.subcategoryId || null;
        this.refId = offer.refId || null;
        this.transportType = offer.transportType || null;
        this.type = offer.type || null;
        this.price = offer.price || null;
        this.promoPrice = offer.promoPrice || null;
        this.isPromo = offer.isPromo || false;
        this.headPhotoIndex = offer.headPhotoIndex || 0;
        this.subcategoryLevel = offer.subcategoryLevel || 0;
        this.sliderLevel = offer.sliderLevel || 0;
        this.isActive = offer.isActive || true;
        this.destinations = offer.destinations || [];
        this.dates = this.mapDates(offer.dates);
        this.photos = offer.photos || [];
        this.hotels = offer.hotels || [];
        this.translations = this.createTranslations(offer.translations);
    }

    createTranslations(translations) {
        let output = [];
        if (!translations) {
            translations = [];
        }

        Object.keys(SITE_TRANSLATIONS_LIST).forEach(lang => {
            let providedTranslation = translations.find(item => item.languageCode === lang);
            if (providedTranslation) {
                output.push(providedTranslation);
            } else {
                output.push(new OfferTranslation(lang));
            }
        });

        return output;
    }

    private mapDates(dates) {
        if (!dates || dates.length === 0) {
            return [];
        }

        return dates.map((date) => {
            let output = {
                dateFrom: null,
                dateTo: null
            };
            if (date.dateFrom) {
                output.dateFrom = resolveDateToDatePicker(date.dateFrom);
            }
            if (date.dateTo) {
                output.dateTo= resolveDateToDatePicker(date.dateTo);
            }
            return output;
        });
    }
}
