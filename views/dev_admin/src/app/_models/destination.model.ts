import { IDestination, IDestinationPhoto } from "../_declarations/main";
import { DestinationTranslation } from "../destination/destination.model";
import { SITE_TRANSLATIONS_LIST } from "../config/app.config";

export class Destination implements IDestination {
    id: number;
    name: string;
    translations: DestinationTranslation[]
    countryId: number;
    photos: IDestinationPhoto[];

    constructor(destination?) {
        destination = destination || {};
        this.id = destination.id || 0;
        // this.bg = destination.bg || new DestinationTranslation('bg');
        // this.en = destination.en || new DestinationTranslation('en');
        // this.ru = hotel.ru || new DestinationTranslation('ru');
        this.translations = this.createTranslations(destination.translations);
        this.countryId = destination.countryId || null;
        this.photos = destination.photos || [];

    }
    createTranslations(translations) {
        let output = [];
        if (!translations) {
            translations = [];
        }
        Object.keys(SITE_TRANSLATIONS_LIST).forEach(lang => {
            let providedTranslation = translations.find(item => item.languageCode === lang);
            if (providedTranslation) {
                output.push(providedTranslation);
            } else {
                output.push(new DestinationTranslation(lang));
            }
        });
        return output;
    }
}
