export class OfferTranslation {

    offerId: number;
    name: string;
    subtitle: string;
    languageCode: string;
    schedule: string;
    itinerary: string;
    pricing: string;
    duration: string;
    promoTitle: string;
    otherInfo: string;

    constructor(lang: string, offer?) {
        offer = offer || {};
        this.offerId = offer.offerId || null;
        this.languageCode = lang;
        this.name = offer.name || '';
        this.subtitle = offer.subtitle || '';
        this.itinerary = offer.itinerary || '';
        this.schedule = offer.schedule || '';
        this.pricing = offer.pricing || '';
        this.duration = offer.duration || '';
        this.promoTitle = offer.promo_title || '';
        this.otherInfo = offer.other_info || '';
    }
}
