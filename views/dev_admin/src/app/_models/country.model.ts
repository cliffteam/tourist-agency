import { ICountry } from "../_declarations/main";

export class Country implements ICountry {

    id: number;
    nameEn: string;
    nameRu: string;
    nameBg: string;
    photo: any;
    descEn: string;
    descRu: string;
    descBg: string;

    constructor(input?) {
        input = input || {};
        this.id = input.id || null;
        this.nameEn = input.nameEn || '';
        this.nameRu = input.nameRu || '';
        this.nameBg = input.nameBg || '';
        this.descEn = input.descEn || '';
        this.descBg = input.descBg || '';
        this.descRu = input.descRu || '';
        this.photo = input.imageUrl ? { path: input.imageUrl } : null;
    }
}
