import { IHotel } from "../_declarations/main";
import { SITE_TRANSLATIONS_LIST } from "../config/app.config";

export class Hotel implements IHotel {
    id: number;
    name: string;
    translations: HotelTranslation[]
    photos: any[]; //TODO: Interface!
    destination: number;
    stars: number;

    constructor(hotel?) {
        hotel = hotel || {};
        this.id = hotel.id || 0;
        this.name = hotel.name || null;
        // this.bg = hotel.bg || new HotelTranslation('bg');
        // this.en = hotel.en || new HotelTranslation('en');
        // this.ru = hotel.ru || new HotelTranslation('ru');
        this.destination = hotel.destination || null;
        this.stars = hotel.stars || null;
        this.translations = this.createTranslations(hotel.translations);
        this.photos = hotel.photos || [];
    }
    createTranslations(translations) {
        let output = [];
        if (!translations) {
            translations = [];
        }
        Object.keys(SITE_TRANSLATIONS_LIST).forEach(lang => {
            let providedTranslation = translations.find(item => item.languageCode === lang);
            if (providedTranslation) {
                output.push(providedTranslation);
            } else {
                output.push(new HotelTranslation(lang));
            }
        });
        return output;
    }
}

export class HotelTranslation {
    hotelId: number;
    languageCode: string;
    description: string;
    pricing: string;

    constructor(lang: string, input?) {
        input = input || {};
        this.hotelId = input.hotelId || null;
        this.languageCode = lang;
        this.description = input.description || '';
        this.pricing = input.pricing || '';
    }
}
