import { ISubcategory } from "../_declarations/main";

export class Subcategory implements ISubcategory {

    id: number;
    nameEn: string;
    nameRu: string;
    nameBg: string;
    categoryId: number;
    photo: any;
    createdOn: Date;

    constructor(input?) {
        input = input || {};
        this.id = input.id || null;
        this.nameEn = input.nameEn || '';
        this.nameRu = input.nameRu || '';
        this.nameBg = input.nameBg || '';
        this.categoryId = input.categoryId || null;
        this.createdOn = input.createdOn || null;
        this.photo = input.imageUrl ? { path: input.imageUrl } : null;
    }
}
