import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';

@Injectable()
export class OffersService {

    offersApiUrl: string = env.apiUrl + 'offers/';

    constructor(private http: HttpClient) { }

    loadList(): Observable<any> {
        return this.http.get(this.offersApiUrl);
    }

    findOne(offerId: number): Observable<any> {
        return this.http.get(this.offersApiUrl + offerId);
    }

    create(data: any): Observable<any> {
        return this.http.post(this.offersApiUrl + 'new', data);
    }

    update(offerId: number, data: any): Observable<any> {
        return this.http.put(this.offersApiUrl + offerId, data);
    }

    delete(offerId: number): Observable<any> {
        return this.http.delete(this.offersApiUrl + offerId);
    }

    deleteImage(imageId: number): Observable<any> {
        return this.http.delete(`${this.offersApiUrl}photos/${imageId}`);
    }
    loadTransportTypes(): Observable<any> {
        return this.http.get(`${this.offersApiUrl}transport-type`);
    }

}
