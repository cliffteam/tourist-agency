import { Component, OnInit } from '@angular/core';
import { OffersService } from '../offers/offers.service';
import { TableData } from '../config/app';
import * as _ from 'lodash';
import { OfferTranslation } from '../_models/offer-translation';

@Component({
    selector: 'app-offers',
    templateUrl: './offers.component.html',
    styleUrls: ['./offers.component.css']
})
export class OffersComponent implements OnInit {
    public offersTable: TableData = {
        headerRow: ['#', 'Име', 'Приоритет', 'Активна'],
        dataRows: []
    };

    constructor(private offersService: OffersService) { }

    ngOnInit() {
        this.loadOffersList();
    }
    resolveName(translations: OfferTranslation) {
        const bgTranslation = _.find(translations, { languageCode: 'bg' });
        return bgTranslation ? bgTranslation.name : '';
    }

    deleteOffer(id: number) {

        if (window.confirm('Сигурни ли сте, че искате да изтриете офертата?')) {
            this.offersService.delete(id).subscribe(result => {
                this.loadOffersList();
            }, error => {
                console.error(error)
            });
        }
    }

    private loadOffersList() {
        this.offersService.loadList().subscribe(response => {
            this.offersTable.dataRows = response.offers;
        });
    }
}
