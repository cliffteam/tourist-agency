import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ValidationErrors, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { OffersService } from '../../offers/offers.service';
import { SubcategoriesService } from '../../subcategories/subcategories.service';
import * as _ from 'lodash';
import { DomSanitizer } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { HotelsService } from '../../hotel/hotel.service';
import { Offer, resolveDateFromDatepicker, resolveDateToDatePicker } from '../../_models/offer.model';
import { DestinationsService } from '../../destination/destination.service';
import { Subscription, Observable } from 'rxjs';
import { IDestination, IOffer, ISubcategory, ICategory, IHotel } from '../../_declarations/main';

export const sortByNameAsc = (a, b) => {
    let result = 0;
    if (a.name > b.name) {
        return -1;
    }
    if (a.name < b.name) {
        return 1;
    }
    return result;
};
@Component({
    selector: 'app-edit-offer',
    templateUrl: './edit-offer.component.html',
    styleUrls: ['./edit-offer.component.css']
})
export class EditOfferComponent implements OnInit, OnDestroy {

    @ViewChild('fileUploadInput', { static: false }) fileUploadInput: ElementRef;

    private subscription: Subscription = new Subscription();

    public destinationsList: IDestination[] = [];
    public offerForm: NgForm;
    public data: IOffer = new Offer();
    public subcategoriesList: ISubcategory[] = [];
    public categoriesList: ICategory[] = [] //['Промоции', 'Екскурзии', 'Почивки', 'Празници', 'Екзотика', 'Круизи'];
    public transportsList: any[] = [];
    public activeTab: string;

    public selectedDestinations: any = [];

    errorMessage: string = null;
    languages: string[] = ['bg', 'en', 'ru'];
    languagesMapping: any = {
        bg: 'български',
        en: 'английски',
        ru: 'руски'
    };
    availableHotels: IHotel[] = [];
    selectedHotelsId: number[] = [];
    hotelsGroupedByDestination: any = []; //TODO: Interface;
    public transportTranslations = {
        bus: 'Автобус',
        plane: 'Самолет',
        cruise: 'Криуз',
        multiple: 'Комбиниран транспорт',
        own: 'Собствен транспорт'
    };

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private offersService: OffersService,
        private subcategoriesService: SubcategoriesService,
        private sanitizer: DomSanitizer,
        private location: Location,
        private hotelsService: HotelsService,
        private destinationsService: DestinationsService
    ) {
        this.activeTab = Object.keys(this.languagesMapping)[0];
    }

    ngOnInit() {
        this.subcategoriesService.loadSubcategoriesList().subscribe(subcategoriesList => {
            this.subcategoriesList = subcategoriesList;
        });
        this.subcategoriesService.loadCategoriesList().subscribe(categoriesList => {
            this.categoriesList = categoriesList;
        });
        this.offersService.loadTransportTypes().subscribe(result => {
            this.transportsList = result.transportTypes;
        });

        this.destinationsService.loadList().subscribe(result => {
            this.destinationsList = result.destinations;

            this.hotelsService.loadList().subscribe(result => {
                const groups = {};
                this.availableHotels = result.hotels;
                result.hotels.forEach((hotel: IHotel) => {
                    if (!groups[hotel.destination]) {
                        groups[hotel.destination] = {
                            id: hotel.destination,
                            name: null,
                            hotels: []
                        };
                    }
                    groups[hotel.destination].hotels.push(hotel);
                });
                Object.keys(groups).forEach(destinationId => {
                    const destination = this.destinationsList.find(destination => destination.id === parseInt(destinationId, 10));
                    const group = groups[destinationId];
                    group.name = destination['name'];
                    this.hotelsGroupedByDestination.push(group);
                });

                this.hotelsGroupedByDestination.sort(sortByNameAsc);
            }, error => {
                console.error(error);
            });
        }, error => {
            console.error(error);
        });

        const sub = this.route.params.subscribe(params => {
            const id = +params.id;
            if (id) {
                this.offersService.findOne(id).subscribe(res => {
                    const offer = res.offer;
                    if (offer) {
                        this.data = new Offer(offer);
                        this.selectedHotelsId = this.data.hotels.map(hotel => hotel.id);
                        this.selectedDestinations = this.data.destinations.map(destinationId => {
                            return {
                                id: destinationId
                            }
                        });
                        for (let i = 0; i < this.data.photos.length; i++) {
                            const photo = this.data.photos[i];
                            photo.isHead = i === this.data.headPhotoIndex;
                        }
                    } else {
                        this.location.back();
                    }
                });
            }
        });
        this.subscription.add(sub);
    }
    addNewTravelDate(): void {
        this.data.dates.push({
            dateFrom: null,
            dateTo: null,
            stay: null
        });
    }
    addNewDestination(): void {
        this.selectedDestinations.push({
            id: null
        });
    }

    isDestionationAlreadyAdded(detinationId) {
        return !!this.selectedDestinations.find(item => item.id === detinationId);
    }

    onUpdateStay(date) {
        let dateFrom = resolveDateFromDatepicker(date.dateFrom);
        dateFrom.setDate(dateFrom.getDate() + date.stay);
        date.dateTo = resolveDateToDatePicker(dateFrom);
    }

    onSubmit(): void {
        let subscribtion: Observable<any>;
        const offer = { ...this.data };
        const formData = new FormData();
        offer.dates = offer.dates.map(item => {
            return {
                dateFrom: resolveDateFromDatepicker(item.dateFrom),
                dateTo: resolveDateFromDatepicker(item.dateTo),
            };
        });

        offer.destinations = this.selectedDestinations
            .filter(destination => !!destination.id)
            .map(destination => destination.id);

        offer.hotels = [];

        this.availableHotels.forEach(hotel => {
            if (this.selectedHotelsId.indexOf(hotel.id) > -1) {
                offer.hotels.push(hotel);
            }
        });

        offer.photos.forEach(photo => {
            if (photo.file) {
                formData.append('photos', photo.file);
                photo.file = null; //Clean file before JSON.stringify;
            }
        });

        formData.append('offer', JSON.stringify(offer));
        if (this.data && this.data.id > 0) {
            subscribtion = this.offersService.update(this.data.id, formData);
        } else {
            subscribtion = this.offersService.create(formData);
        }
        subscribtion.subscribe(result => {
            if (result.success === true) {
                this.router.navigate(['/offers']);
            } else {
                this.errorMessage = result.msg || result.message;
            }
        });
    }

    onImagesChange(event) {
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            let file = event.target.files[0];
            reader.onload = (event: any) => {
                let newImage = {
                    path: window.URL.createObjectURL(file),
                    file: file,
                    isHead: false
                };
                this.data.photos.push(newImage);
            }

            this.fileUploadInput.nativeElement.value = null;
            reader.readAsDataURL(file);
        }
    }

    deleteImage(image: any, index: number) {
        if (image.id) {
            if (window.confirm('Сигурни ли сте, че искате да изтриете снимката?')) {
                this.offersService.deleteImage(image.id).subscribe(result => {
                    this.data.photos.splice(index, 1);
                }, error => {
                    console.error('>>>erorr', error);
                    alert('Error in image delete');
                });
            }
        } else {
            this.data.photos.splice(index, 1);
        }
    }

    sanitize(url: string) {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${url})`);
    }

    cancel() {
        this.location.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    makeHead(photo, index) {
        for (let i = 0; i < this.data.photos.length; i++) {
            this.data.photos[i].isHead = false;
        }
        this.data.headPhotoIndex = index;
        photo.isHead = true;

    }
    // For test purposes only;
    getFormValidationErrors(form) {
        Object.keys(form.controls).forEach(key => {

            const controlErrors: ValidationErrors = form.form.get(key).errors;
            if (controlErrors != null) {
                Object.keys(controlErrors).forEach(keyError => {
                    console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                });
            }
        });
    }
}