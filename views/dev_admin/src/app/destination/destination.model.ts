export class DestinationTranslation {
    destinationId: number;
    countryId: number;
    name: string;
    description: string;
    usefulInfo: string;
    languageCode: string;

    constructor(lang: string, input?) {
        input = input || {};
        this.destinationId = input.destinationId || null;
        this.countryId = input.countryId || null;
        this.languageCode = lang;
        this.description = input.description || '';
        this.name = input.name || '';
        this.usefulInfo = input.usefulInfo || '';
    }
}