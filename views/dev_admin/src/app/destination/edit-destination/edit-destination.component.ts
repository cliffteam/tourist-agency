import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { IDestination, ICountry } from '../../_declarations/main';
import { Destination } from '../../_models/destination.model';
import { SITE_TRANSLATIONS_LIST } from '../../config/app.config';
import { Location } from '@angular/common';
import { Subscription, Observable } from 'rxjs';
import { DestinationsService } from '../destination.service';
import { CountriesService } from '../../country/country.service';

@Component({
    selector: 'app-edit-destination',
    templateUrl: './edit-destination.component.html',
    styleUrls: ['./edit-destination.component.css']
})
export class EditDestinationComponent implements OnInit, OnDestroy {
    @ViewChild('fileUploadInput', {static: false}) fileUploadInput: ElementRef;

    destinationForm: FormGroup;
    sub: Subscription;
    data: IDestination = new Destination();
    public countriesList: ICountry[] = [];
    public activeTab: string;

    languages: string[];
    languagesMapping: any = SITE_TRANSLATIONS_LIST;
    errorMessage: string = null;

    constructor(
        private router: Router,
        private location: Location,
        private route: ActivatedRoute,
        private destinationsService: DestinationsService,
        private countriesService: CountriesService,
        private sanitizer: DomSanitizer
    ) {
        const langCodes = Object.keys(this.languagesMapping);
        this.activeTab = langCodes[0];
        this.languages = langCodes;
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            const id = +params.id;
            if (id && id > 0) {
                this.destinationsService.findOne(id).subscribe(res => {
                    let destination = res.destination;
                    if (destination) {
                        this.data = new Destination(destination);
                    } else {
                        // this.location.back();
                    }
                });
            }
        });
        this.loadCountriesList();
    }

    onSubmit(): void {
        this.errorMessage = null;
        let subscribtion: Observable<any>;
        let destination = { ...this.data };

        let formData = new FormData();

        destination.photos.forEach(photo => {
            if (photo.file) {
                console.log(photo);
                formData.append('photos', photo.file);
                photo.file = null; //Clean file before JSON.stringify;
            }
        });

        formData.append('destination', JSON.stringify(destination));

        if (this.data && this.data.id > 0) {
            subscribtion = this.destinationsService.update(this.data.id, formData);
        } else {
            subscribtion = this.destinationsService.create(formData);
        }

        subscribtion.subscribe(result => {
            if (result.success === true) {
                this.router.navigate(['/destinations']);
            } else {
                this.errorMessage = result.msg || result.message;
            }
        });
    }

    onImagesChange(event) {
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            let file = event.target.files[0];
            reader.onload = (event: any) => {
                const newImage = {
                    path: window.URL.createObjectURL(file),
                    file: file
                };
                this.data.photos.push(newImage);
            }

            reader.readAsDataURL(file);
            this.fileUploadInput.nativeElement.value = null;
        }
    }

    deleteImage(image: any, index: number) {
        if (image.id) {
            if (window.confirm('Сигурни ли сте, че искате да изтриете снимката?')) {
                this.destinationsService.deleteImage(image.id).subscribe(result => {
                    this.data.photos.splice(index, 1);
                }, error => {
                    console.error('>>>erorr', error);
                    alert('Error in image delete');
                })
            }
        } else {
            this.data.photos.splice(index, 1);
        }
    }
    sanitize(url: string) {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${url})`);
    }

    private loadCountriesList() {
        this.countriesService.loadList().subscribe(result => {
            this.countriesList = result.result;
        }, console.error);
    }

    /*mapDestinationTranslations(destination: IDestination) {
        destination.translations.forEach((transition: DestinationTranslation) => {
            destination[transition.languageCode] = transition;
        });
        return destination;
    }*/

    cancel() {
        this.location.back();
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
}
