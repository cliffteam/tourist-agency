import { Component, OnInit, OnDestroy } from '@angular/core';
import { DestinationsService } from './destination.service';
import { IDestination } from '../_declarations/main';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-destination',
    templateUrl: './destination.component.html',
    styleUrls: ['./destination.component.css']
})
export class DestinationComponent implements OnInit, OnDestroy {
    private subscription: Subscription = new Subscription();
    public destinationsList: IDestination[] = [];

    constructor(
        private destinationsService: DestinationsService,
    ) { }

    ngOnInit() {
        this.loadList();
    }

    deleteDestination(destination: IDestination) {
        const confirmDelete = window.confirm(`Are you sure you want to delete destination with ID: ${destination.id}?`);
        if (confirmDelete) {
            const sub = this.destinationsService.delete(destination.id).subscribe(response => {
                this.loadList();
            }, error => {
                console.error('Delete Dynamic Template:', error.error.msg);
            });
            //TODO: Error handling with Notifications
            this.subscription.add(sub);
        }
    }
    private loadList() {
        this.destinationsService.loadList().subscribe(result => {
            console.log('>>loadList>', result);
            this.destinationsList = result.destinations;
        }, console.error);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
