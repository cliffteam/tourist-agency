import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';

@Injectable()
export class DestinationsService {

    destinationsApiUrl: string = env.apiUrl + 'offers/destinations/';

    constructor(private http: HttpClient) { }

    loadList(): Observable<any> {
        return this.http.get(this.destinationsApiUrl);
    }

    findOne(destinationId: number): Observable<any> {
        return this.http.get(this.destinationsApiUrl + destinationId);
    }

    create(data: any): Observable<any> {
        return this.http.post(this.destinationsApiUrl, data);
    }

    update(destinationId: number, data: any): Observable<any> {
        console.log("UPDAGING")
        return this.http.put(this.destinationsApiUrl + destinationId, data);
    }

    delete(destinationId: number): Observable<any> {
        return this.http.delete(this.destinationsApiUrl + destinationId);
    }

    deleteImage(imageId: number): Observable<any> {
        return this.http.delete(`${this.destinationsApiUrl}photos/${imageId}`);
    }

}
