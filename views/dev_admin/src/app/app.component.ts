import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { UserService } from './user/user.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    constructor(
        public location: Location,
        private userService: UserService
    ) { }

    ngOnInit() {
    }

    isMap(path) {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        titlee = titlee.slice(1);
        if (path == titlee) {
            return false;
        }
        else {
            return true;
        }
    }
    isLoggedUser(): boolean {
        return this.userService.isUserLogged();
    }
}
